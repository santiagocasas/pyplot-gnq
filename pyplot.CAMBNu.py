import matplotlib.pyplot as plt
import numpy as np
import sys
import copy
import matplotlib
import matplotlib.ticker as ticker
import matplotlib.gridspec as gridspec
from scipy import interpolate
import tools

def ticks_format(value, index):
    """
    This function decompose value in base*10^{exp} and return a latex string.
    If 0<=value<99: return the value as it is.
    if 0.1<value<0: returns as it is rounded to the first decimal
    otherwise returns $base*10^{exp}$
    I've designed the function to be use with values for which the decomposition
    returns integers
    """
    exp = np.floor(np.log10(value))
    base = value/10**exp
    if exp == 0 or exp == 1:
        return '${0:d}$'.format(int(value))
    if exp == -1:
        return '${0:.1f}$'.format(value)
    else:
        return '${0:d}\\times10^{{{1:d}}}$'.format(int(base), int(exp))    
    
def extract_columns(data, quant=0):    
    if (quant==0 or quant==1):
        a = data[:,0]
        b = data[:,1]
        c = data[:,2]
        return a, b, c
    elif (quant==2):
        a = data[:,0]
        b = data[:,1]
        c = data[:,2]
        d = data[:,3]
        return a, b, c, d
    
def w_eq_of_state(q, q_dot, hubble=1, scale=1, camb=False):
    alpha = 10.
    V0 = 1.06e-7
    kine = 0.5*q_dot**2
    if camb:
        print len(q)
        print len(q_dot)
	print len(hubble)
	print len(scale)
        kine=kine*(hubble**2)*(scale**2)
    p = kine-V0*np.exp(-alpha*q)
    rho = kine+V0*np.exp(-alpha*q)
    w = p/rho   
    return rho, p, w
    
def z_redshift(a):
    z = (1.0/a) - 1.0
    return z
def rho_total(rho1, rho2, rho3):
    rhotot=rho1+rho2+rho3
    return rhotot
def Omega_calc(rhoi, rhotot):
    Omegai=rhoi/rhotot
    return Omegai  
    
#Base5 = './background-2em3/' 
#Base4 = './background-9em4/' 
#Base3 = './background-ng64-alpha11-new/' 
#Base2 = './background-ng64-alpha9-new/'
#Base1 = './background-2em4/'



#simulabel1 = '2em4'
#simulabel2 = 'alpha9'
#simulabel3 = 'alpha11'
#simulabel4 = '9em4'
#simulabel5 = '2em3'
#simulabel5 = 'GNQ64-std-vb-loc-'

#baselist=[Base1, Base2, Base3]

#simulablist = [simulabel1, simulabel2, simulabel3]

plotwhat="Alsocamb"  #choose what or not to plot

# lof = list of files
#background_from = ['background_general.dat','background_q.dat','background_nu.dat']

#list_of_back_files = [[base+filename for filename in background_from] for base in baselist] 

#choose data set 
#data_full = [[np.loadtxt(bckfile)[:,:] for bckfile in bckfiles] for bckfiles in list_of_back_files]


#BaseCamb = './camb-data-input-full/nominal-camb-alpha10p0-mnu2em4/'

BaseCamb = './nucambdata/'

alphapar='alpha10'
masspar='model4'

modelname=alphapar+'_'+masspar
quanttype='bg_varbeta_'
simtype=quanttype+modelname+'_'

backgr = ['H.dat','q.dat','qdot.dat','rho_cdm.dat','rho_nu.dat','p_nu.dat']
backgr_files=[BaseCamb+simtype+backi for backi in backgr]
data_camb = [np.loadtxt(bfile)[:,:] for bfile in backgr_files]


#--Extract CAMB data-- #
a_camb = data_camb[0][:,0]
curlH_camb = data_camb[0][:,1]
H_camb = curlH_camb/a_camb

rho_m_camb = data_camb[3][:,1]
rho_nu_camb = data_camb[4][:,1]

p_nu_camb = data_camb[5][:,1]

q_camb = data_camb[1][:,1]
qdot_camb = data_camb[2][:,1]

rho_q_camb, p_q_camb, w_q_camb = w_eq_of_state(q_camb, qdot_camb, hubble=H_camb, scale=a_camb, camb=True)

rho_total_camb = rho_total(rho_m_camb, rho_nu_camb, rho_q_camb) 

Omega_m_camb = Omega_calc(rho_m_camb,rho_total_camb)

Omega_nu_camb = Omega_calc(rho_nu_camb,rho_total_camb)

Omega_q_camb = Omega_calc(rho_q_camb, rho_total_camb)

w_nu_camb = p_nu_camb/rho_nu_camb

#units2=Omega_m_camb[0]+Omega_nu_camb[0]+Omega_q_camb[0]

H_func = interpolate.pchip(a_camb, H_camb)
rho_m_func = interpolate.pchip(a_camb, rho_m_camb)
Omega_m_func = interpolate.pchip(a_camb, Omega_m_camb) 
q_func =  interpolate.pchip(a_camb,q_camb)
w_q_func =  interpolate.pchip(a_camb,w_q_camb)
Omega_nu_func = interpolate.pchip(a_camb, Omega_nu_camb) 
Omega_q_func = interpolate.pchip(a_camb, Omega_q_camb) 


H0units=1.0/300000

rho_m_0 = float(rho_m_func(1.0))
H_0 = H_func(1.0)/H0units
Omega_m_0 = float(Omega_m_func(1.0))
Omega_nu_0 = float(Omega_nu_func(1.0))
Omega_q_0 = float(Omega_q_func(1.0))
q_0 = float(q_func(1.0))
w_q_0 = float(w_q_func(1.0))


omega_file = './camb-background-plots/GNQ-CAMB-Omegas--'+modelname+'.txt'

ind = np.where(a_camb <= 1.0)

omega_data_out = np.column_stack((a_camb[ind][::-1], (H_camb/H0units)[ind][::-1], Omega_q_camb[ind][::-1], Omega_nu_camb[ind][::-1], Omega_m_camb[ind][::-1], w_q_camb[ind][::-1], w_nu_camb[ind][::-1]))


np.savetxt(omega_file, omega_data_out, fmt='%5.5e', header="a,        H,        Omega_phi,        Omega_nu,        Omega_m,        w_phi,        w_nu")


print("File saved")

###-------------------------##
a_s=[]
H_a_s=[]
rho_m_s=[]
rho_nu_s=[]
p_nu_s=[]
m_bar_nu_s=[]
w_nu_s=[]
qui_s=[]
qui_dot_s=[]
rho_q_s=[]
p_q_s=[]
w_q_s=[]
Omega_m_s=[]
Omega_nu_s=[]
Omega_q_s=[]
w_eff_s=[]
H0_s=[]
units_s=[]


#for j, databck in enumerate(data_full):
#
#    a, H_a, rho_m = extract_columns(databck[0], quant=0)
#    a, rho_nu, p_nu, m_bar_nu = extract_columns(databck[2], quant=2)
#    a, qui, qui_dot = extract_columns(databck[1], quant=1)
#    
#    Omega_m = Omega_calc(rho_m, H_a)
#
#    Omega_nu = Omega_calc(rho_nu, H_a)
#
#    rho_q, p_q, w_q = w_eq_of_state(qui, qui_dot)
#
#    Omega_q = Omega_calc(rho_q, H_a)
#
#    w_nu = p_nu/rho_nu
#    w_eff = (w_q*Omega_q+w_nu*Omega_nu)/(Omega_nu+Omega_q)
#
#    units=Omega_m[0]+Omega_nu[0]+Omega_q[0]
#
#    H0 = H_a[-1]
#    
#    a_s.append(a)
#    H_a_s.append(H_a)
#    rho_m_s.append(rho_m)
#    rho_nu_s.append(rho_nu)
#    p_nu_s.append(p_nu)
#    m_bar_nu_s.append(m_bar_nu)
#    w_nu_s.append(w_nu)
#    qui_s.append(qui)
#    qui_dot_s.append(qui_dot)
#    rho_q_s.append(rho_q)
#    p_q_s.append(p_q)
#    w_q_s.append(w_q)
#    Omega_m_s.append(Omega_m)
#    Omega_nu_s.append(Omega_nu)
#    Omega_q_s.append(Omega_q)
#    w_eff_s.append(w_eff)
#    H0_s.append(H0)
#    units_s.append(units)


#Omega_m_0 = 0.204


#H0lcdm=0.000228173
#Omegal=0.7

#kappa = rho_m_0/(Omega_m_0*H0**2)

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

G = gridspec.GridSpec(4,4)
#choose image size, dpi and background color
fig=plt.figure(1, figsize=(20,12), dpi=80,facecolor='w')
#fig2 = plt.figure(2, figsize=(20,12), dpi=80, facecolor='w')
#list of default colors for plot lines
collist=['b','g','r','c','Indigo','Olive','OrangeRed','SkyBlue', 'ForestGreen', 'DarkGoldenrod', 'GoldenRod', 'Plum', 'Crimson', 'SteelBlue', 'YellowGreen']
collist2=['Indigo','Olive','OrangeRed','SkyBlue', 'ForestGreen', 'DarkGoldenrod', 'GoldenRod', 'Plum', 'Crimson', 'SteelBlue', 'YellowGreen']
lslis=['-','--',':','-.','.']
# specify plot grid axes
if (plotwhat=="Alsocamb"):
    axes1 = fig.add_subplot(G[:1,:])

    axes1.loglog(a_camb,rho_m_camb, '-', label='$\\rho_m$ camb', color=collist2[0], lw=2, ms=6)
    axes1.loglog(a_camb,rho_nu_camb, '-', label='$\\rho_{\\nu}$ camb', color=collist2[1], lw=2, ms=6)
    axes1.loglog(a_camb,rho_q_camb, '-', label='$\\rho_{\\phi}$ camb', color=collist2[2], lw=2, ms=6)


    indi=0

    #for a, rho_m, rho_nu, rho_q in zip(a_s, rho_m_s, rho_nu_s, rho_q_s):
    #    indj=0
    #    axes1.loglog(a,rho_m, lslis[indi], label=simulablist[indi]+'$\\rho_m$', color=collist[indj], lw=3, ms=6)
    #    indj=1
    #    axes1.loglog(a,rho_nu, lslis[indi], label=simulablist[indi]+'$\\rho_{\\nu}$', color=collist[indj], lw=3, ms=6)
    #    indj=2
    #    axes1.loglog(a,rho_q, lslis[indi], label=simulablist[indi]+'$\\rho_{\\phi}$', color=collist[indj], lw=3, ms=6)
    #    indi+=1

    xmini,xmaxi,ymini,ymaxi = axes1.axis('tight')
    axes1.axis(ymax=ymaxi, xmin=0.01, xmax=1.2, ymin=ymini)

    axes1.legend(loc='best',markerscale=1.0,prop={'size':16},numpoints=3,handlelength=3)
    axes1.grid(True,which="major",ls=":")
    axes1.tick_params(length=6, width=1, labeltop=True)
    axes1.set_ylabel("$\\bar\\rho (a)$",size='x-large')
    axes1.xaxis.set_label_position('top')
    axes1.set_xlabel("$a$",size='x-large')

    axes3 = fig.add_subplot(G[1:2,:])
                                                                                                     
    axes3.semilogx(a_camb,q_camb, '-', label='$\\phi$', color=collist2[-1], lw=2, ms=6)
    axes3.semilogx(a_camb,qdot_camb, '--', label='$\\dot\\phi$', color=collist2[-2], lw=2, ms=6)
    axes3.semilogx(a_camb,w_q_camb, '-.', label='$w_{\\phi}$', color=collist2[-3], lw=2, ms=6)
    axes3.semilogx(a_camb,w_nu_camb+1.0, '--', label='$w_{\\nu}+1$', color=collist2[-4], lw=2, ms=6)

    xmini,xmaxi,ymini,ymaxi = axes3.axis('tight')
    axes3.axis(ymax=1.5*np.abs(q_camb[0]), xmin=0.01, xmax=1.2, ymin=1.5*ymini)

    axes3.legend(loc='best',markerscale=1.0,prop={'size':14},numpoints=2,handlelength=2)
    axes3.grid(True,which="major",ls=":")
    axes3.tick_params(length=6, width=1, labeltop=True)
    axes3.set_ylabel("$\\bar\\phi (a)$",size='x-large')
    axes3.xaxis.set_label_position('top')
    axes3.set_xlabel("$a$",size='x-large')




    subs = [1., 2., 4., 6., 8.]

    axes2 = fig.add_subplot(G[2:,:])

    #axes2.plot(a_s[0],(Omegal*H0lcdm**2)/H_a_s[0]**2, '--', label='$\\Omega_{\\Lambda 0}=0.7$', color=collist[4], lw=2, ms=6)

    #indi=0
    #for a, Omega_m, Omega_nu, Omega_q, units in zip(a_s, Omega_m_s, Omega_nu_s, Omega_q_s, units_s): 
    #    indj=0
    #    axes2.plot(a,Omega_m/units, lslis[indi], label=simulablist[indi]+'$\\Omega_m$', color=collist[indj], lw=2, ms=6)
    #    indj=1
    #    axes2.plot(a,Omega_nu/units, lslis[indi], label=simulablist[indi]+'$\\Omega_{\\nu}$', color=collist[indj], lw=2, ms=6)
    #    indj=2
    #    axes2.plot(a,Omega_q/units, lslis[indi], label=simulablist[indi]+'$\\Omega_{\\phi}$', color=collist[indj], lw=2, ms=6)
    #    indi+=1   

    axes2.plot(a_camb,Omega_m_camb, '-.', label='$\\Omega_m$', color=collist2[0], lw=3, ms=6)
    axes2.plot(a_camb,Omega_nu_camb, '-.', label='$\\Omega_{\\nu}$', color=collist2[1], lw=3, ms=6)

    axes2.plot(a_camb,Omega_q_camb, '-.', label='$\\Omega_{\\phi}$', color=collist2[2], lw=3, ms=6)
    axes2.plot(a_camb,H_camb/H0units/100, '--', label='$H(a)$', color=collist2[3], lw=3, ms=6)


    #xderi, yderi, xderi2, yderi2 = tools.derivatives(x, y, order=2)



    xmini,xmaxi,ymini,ymaxi = axes2.axis('tight')
    axes2.axis(ymax=1.1, xmin=0.1, xmax=1.2, ymin=0)

    axes2.legend(loc='best',markerscale=1.0,prop={'size':14},numpoints=3,handlelength=3)
    axes2.grid(True,which="major",ls=":")
    axes2.tick_params(length=6, width=1, labeltop=True)
    axes2.set_ylabel("$\\bar\\Omega_{i} (a), H(a)$",size='x-large')
    axes2.xaxis.set_label_position('bottom')
    axes2.set_xlabel("$a$",size='x-large')

else:
    print "Not plotting species densities and camb input"

#ratioArray1 = tools.takeRatio(datalist0[0],datalist1[0])
#ratioArray2 = tools.takeRatio(datalist0[0],datalist2[0])
#ratioArray3 = tools.takeRatio(datalist4[0],datalist0[0])

rho_m_0="{:.3f}".format(rho_m_0)
H_0="{:.2f}".format(H_0)
Omega_m_0="{:.2f}".format(Omega_m_0)
Omega_nu_0="{:.3f}".format(Omega_nu_0)
Omega_q_0="{:.3f}".format(Omega_q_0)
q_0="{:.3f}".format(q_0)
w_q_0="{:.3f}".format(w_q_0)

fig.suptitle(alphapar+'--'+masspar+' parameters z=0: '+'Omegaphi='+Omega_q_0+', Omegam='+Omega_m_0+', Omeganu='+Omega_nu_0+', H0='+H_0, fontsize=16)


figsavename='./nucambplots/GNQ-NuCAMB-Omega-Densities--'+modelname+'.png'

fig.savefig(figsavename,dpi=200)

plt.show()

#plt.close(fig)
#plt.close()


###--possibly useful code--#

#axes1.text(0.02,0.1,'z=0',verticalalignment='top',horizontalalignment='left',transform=axes1.transAxes,fontsize=14,style='italic') 
#axes1.xaxis.set_minor_locator(ticker.LogLocator(subs=subs))
#axes1.xaxis.set_major_formatter(ticker.NullFormatter())
#axes1.xaxis.set_minor_formatter(ticker.FuncFormatter(ticks_format))
#axes1.xaxis.set_minor_formatter(ticker.LogFormatter(base=10.0, labelOnlyBase=False))
#minorLocator   = ticker.LogLocator(0.1,[0.01,0.02])
#axes1.xaxis.set_minor_locator(minorLocator)
#axes1.set_xticks(np.linspace(0.5,1.0,5))
#axes1.set_xticklabels(('0.1','0.2','0.4','0.5','0.6','0.7','0.8''0.9'))

###quantity=0 #0=general, 1=quintessence, 2=neutrinos
###delta_a = 0.01
###a_init = 0.02
###a_fin = 1.0
###a_values = np.arange(int(a_init*100)+1, int(a_fin*100), int(delta_a*100)) # or +1
###list_zerofill = [str(av).zfill(3) for av in a_values ]
###list_snaps = [aval.rstrip('0') for aval in list_zerofill]


