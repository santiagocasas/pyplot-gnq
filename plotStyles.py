import itertools

#list of default colors for plot lines
collist=['b','g','r','c','Indigo','Olive','OrangeRed','SkyBlue', 'ForestGreen', 'DarkGoldenrod', 'GoldenRod', 'Plum', 'Crimson', 'SteelBlue', 'YellowGreen']
collist2=['Indigo','OrangeRed','SkyBlue', 'ForestGreen', 'DarkGoldenrod', 'Plum', 'Crimson', 'SteelBlue', 'YellowGreen']
collist3 = ['firebrick', 'limegreen', 'darkorange', 'dodgerblue', 'gold', 'darkorchid', 'peru', 'deeppink']

colstand2=['darkorange','dodgerblue']
colstand3=['firebrick','dodgerblue']

lslis0=['--','-',':','-.','.']
lslis=['-','--',':','-.','.']
lslis2=lslis[::-1]

cycol = itertools.cycle(collist2)
cylin = itertools.cycle(lslis)

bbox_props = dict(boxstyle="round", fc="w", ec="0.5", alpha=0.9)

def saveplots(question, imagef, figure):
    if (question==True):
        figure.savefig(imagef+".png",dpi=400, bbox_inches='tight')
        print "saving png"
        figure.savefig(imagef+".pdf",dpi=400, bbox_inches='tight')
        print "saving pdf"
        figure.savefig(imagef+".eps",format="eps", dpi=1000, bbox_inches='tight')
        print "saving ps"
    else:
        print "no plots saved"

