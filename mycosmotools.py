import numpy as np
#import numpy as np
# mycosmotools module 

def w_eq_of_state(q, q_dot, V0=0.99e-7, alpha=10,  hubble=1, scale=1, camb=False):
    kine = 0.5*q_dot**2
    #print len(q)
    #print len(q_dot)
	#print len(hubble)
	#print len(scale)
    kine=kine*(hubble**2)*(scale**2)
    p = kine-V0*np.exp(-alpha*q)
    rho = kine+V0*np.exp(-alpha*q)
    w = p/rho   
    return rho, p, w
    
def z_redshift(a):
    z = (1.0/a) - 1.0
    return z

def rho_total(rho1, rho2, rho3):
    rhotot=rho1+rho2+rho3
    return rhotot

def Omega_calc(rhoi, rhotot):
    Omegai=rhoi/rhotot
    return Omegai  

H0units = 100/299792 
