import os
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import sys

class Printer():
    """
    Print things to stdout on one line dynamically
    """
 
    def __init__(self,data):
 
        sys.stdout.write("\r\x1b[K"+data.__str__())
        sys.stdout.flush()

## Step1: Write here the path to the folder that contains the snapshots
time_1_folder='../2019-08-08_10:45:34/'


## Step2: Write here the path to the output folder, it will be created below if nonexistent.
npsavedir='./gnq-model2-ini-pydata/'


## Step3: This should correspond to the same number as in parameters.cpp 'm_nu_eV_factor'. It is just a model-dependent change of units.
m_eV_fact = 1.5045


## Step 4: Name of the snapshot files. For DM particles change to part_m.bin
particles_file='part_nu.bin'
times_file='times.bin'


ptfile1 = time_1_folder+particles_file
tifile1 = time_1_folder+times_file

## Step 5: Write next to ",3,3" the number of particles obtained by running this code once and pressing "no" when prompted.
dt_parts=np.dtype("int64, (4194242,3,3)float32")
dt_time = np.dtype("float64")


parts_arra1 = np.fromfile(ptfile1, dtype=dt_parts)
time_arra1 = np.fromfile(tifile1, dtype=dt_time)


Np1=parts_arra1[0][0]
atime1 = time_arra1[0]
ztime1 = 1.0/atime1 - 1.0
clock_global_t = time_arra1[1]

print "Number of neutrino particles: "
print "Np1: ", Np1
print "Cosmological time of snapshot: "
print "a: ", atime1
print "z: ", ztime1
print "Global clock time of snapshot: "
print "seconds: ", "{:.2f}".format(clock_global_t)

confi = raw_input('Would you like to continue? (yes/no): ')
if confi.lower() == 'yes':
  print "proceeding to extract arrays"
else:
  print "exiting"
  sys.exit(1)

## create output folder
if not os.path.exists(npsavedir):
    os.makedirs(npsavedir)

nu_parts1= parts_arra1[0][1]


v2_arra = []
gamma_arra = []  
mass_arra = []

v_arra = []
r_arra = []

counter=1.0

for nu in nu_parts1:
    r_arra.append(nu[0])
    v_arra.append(nu[1])
    v2=np.sum(np.square(nu[1]))
    v2_arra.append(v2)
    mass = nu[2][0]*m_eV_fact  ## without the N factor as in m*N*factor stated in particle.cpp
    mass_arra.append(mass)
    gamma = nu[2][2]
    gamma_arra.append(gamma)
    currperc = (counter/Np1)*100
    output = "%f %% of %d completed." % (currperc,Np1)
    Printer(output)
    counter += 1

print "extraction of arrays finished"

print "converting arrays to numpy arrays"

r_nparra = np.array(r_arra)
v_nparra = np.array(v_arra)

v2_nparra = np.array(v2_arra)
gamma_nparra = np.array(gamma_arra)
mass_nparra = np.array(mass_arra)

print "saving arrays to directory: ", npsavedir

np.save(npsavedir+'positionArra3.npy', r_nparra)
np.save(npsavedir+'velocityArra3.npy', v_nparra)
np.save(npsavedir+'velosquaredArra1.npy', v2_nparra)
np.save(npsavedir+'massArra1.npy', mass_nparra)
np.save(npsavedir+'gammaArra1.npy', gamma_nparra)

print "finished: Goodbye"


