import matplotlib.pyplot as plt
import numpy as np
import sys
import copy
import matplotlib
import matplotlib.ticker as ticker
import matplotlib.gridspec as gridspec
from scipy import interpolate
import tools

def ticks_format(value, index):
    """
    This function decompose value in base*10^{exp} and return a latex string.
    If 0<=value<99: return the value as it is.
    if 0.1<value<0: returns as it is rounded to the first decimal
    otherwise returns $base*10^{exp}$
    I've designed the function to be use with values for which the decomposition
    returns integers
    """
    exp = np.floor(np.log10(value))
    base = value/10**exp
    if exp == 0 or exp == 1:
        return '${0:d}$'.format(int(value))
    if exp == -1:
        return '${0:.1f}$'.format(value)
    else:
        return '${0:d}\\times10^{{{1:d}}}$'.format(int(base), int(exp))    
    
def extract_columns(data, quant=0):    
    if (quant==0 or quant==1):
        a = data[:,0]
        b = data[:,1]
        c = data[:,2]
        return a, b, c
    elif (quant==2):
        a = data[:,0]
        b = data[:,1]
        c = data[:,2]
        d = data[:,3]
        return a, b, c, d
    
def w_eq_of_state(q, q_dot, hubble=1, scale=1, camb=False):
    alpha = 10.
    V0 = 1.06e-7
    kine = 0.5*q_dot**2
    if camb:
        print len(q)
        print len(q_dot)
	print len(hubble)
	print len(scale)
        kine=kine*(hubble**2)*(scale**2)
    p = kine-V0*np.exp(-alpha*q)
    rho = kine+V0*np.exp(-alpha*q)
    w = p/rho   
    return rho, p, w
    
def z_redshift(a):
    z = (1.0/a) - 1.0
    return z

def rho_total(rho1, rho2, rho3):
    rhotot=rho1+rho2+rho3
    return rhotot

def Omega_calc(rhoi, rhotot):
    Omegai=rhoi/rhotot
    return Omegai  
    
#Base1 = './background-64-alpha9-2em4/' 
#Base2 = './background-64-alpha10-3em4/'
#Base3 = './background-64-alpha10-5em5/'
#Base4 = './background-64-alpha10-6em4/'
#Base5 = './background-64-alpha11-2em4/'
#Base6 = './background-128-alpha10-3em4/'
#Base7 = './background-128-alpha10-6em4/'
#Base8 = './background-128-alpha11-2em4/'

#Base00 = './background-64-alpha10-2em4/'
#Base01 = './background-128-alpha10-2em4/'

Base1 = './background-model3-alfa10-64/'




#simulabel1 = 'n64-alpha9-m2em4'
#simulabel2 = 'n64-alpha10-m3em4'
#simulabel3 = 'n64-alpha10-m5em5' 
#simulabel4 = 'n64-alpha10-m6em4'
#simulabel5 = 'n64-alpha11-m2em4'
#
#simulabel6 = 'n128-alpha10-m3em4'
#simulabel7 = 'n128-alpha10-m6em4'
#simulabel8 = 'n128-alpha11-m2em4'
#simulabel00 = 'n64-alpha10-m2em4'
#simulabel01 = 'n128-alpha10-m2em4'

simulabel1 = 'n64-Mc-model3'

baselist=[Base1]

simulablist = [simulabel1]

imagefile='./simulation-plots/GNQ-Mc-64-nu-alpha10-m2em4-v3.png'

plotwhat="Nocamb"  #choose what or not to plot

# lof = list of files
background_from = ['background_general.dat','background_q.dat','background_nu.dat']

list_of_back_files = [[base+filename for filename in background_from] for base in baselist] 

#choose data set 
data_full = [[np.loadtxt(bckfile)[:,:] for bckfile in bckfiles] for bckfiles in list_of_back_files]

#plot quantities until z=0 plotend=0 or until last value of files plotend=1
plotend=1


H0units=1.0/299792

###-------------------------##
a_s=[]
H_a_s=[]
rho_m_s=[]
rho_nu_s=[]
rho_tot_s=[]
p_nu_s=[]
m_bar_nu_s=[]
w_nu_s=[]
qui_s=[]
qui_dot_s=[]
rho_q_s=[]
p_q_s=[]
w_q_s=[]
Omega_m_s=[]
Omega_nu_s=[]
Omega_q_s=[]
w_eff_s=[]
H0_s=[]
units_s=[]
rho_m_0_s=[]
Omega_m_0_s=[]
Omega_nu_0_s=[]
Omega_q_0_s=[]
q_0_s=[]
w_q_0_s=[]
m_bar_nu_0_s=[]

for j, databck in enumerate(data_full):

    a, H_a, rho_m = extract_columns(databck[0], quant=0)
    a, rho_nu, p_nu, m_bar_nu = extract_columns(databck[2], quant=2)
    a, qui, qui_dot = extract_columns(databck[1], quant=1)
    
    rho_q, p_q, w_q = w_eq_of_state(qui, qui_dot)

    rho_total_simu = rho_total(rho_m, rho_nu, rho_q) 
    
    Omega_m = Omega_calc(rho_m, rho_total_simu)

    Omega_nu = Omega_calc(rho_nu, rho_total_simu)

    Omega_q = Omega_calc(rho_q, rho_total_simu)

    w_nu = p_nu/rho_nu
    w_eff = (w_q*Omega_q+w_nu*Omega_nu)/(Omega_nu+Omega_q)

    if plotend==1 :
        afinal=a[-1]
    else plotend==0 : 
        afinal=1.0

    H0 = float(interpolate.interp1d(a, H_a)(afinal))/H0units
    Omega_m_0 = float(interpolate.interp1d(a, Omega_m)(afinal))
    Omega_nu_0 = float(interpolate.interp1d(a, Omega_nu)(afinal))
    Omega_q_0 = float(interpolate.interp1d(a, Omega_q)(afinal))
    m_bar_nu_0 = float(interpolate.interp1d(a, m_bar_nu)(afinal))
 
    
    a_s.append(a)
    H_a_s.append(H_a)
    rho_m_s.append(rho_m)
    rho_nu_s.append(rho_nu)
    p_nu_s.append(p_nu)
    m_bar_nu_s.append(m_bar_nu)
    w_nu_s.append(w_nu)
    qui_s.append(qui)
    qui_dot_s.append(qui_dot)
    rho_q_s.append(rho_q)
    p_q_s.append(p_q)
    w_q_s.append(w_q)
    Omega_m_s.append(Omega_m)
    Omega_nu_s.append(Omega_nu)
    Omega_q_s.append(Omega_q)
    w_eff_s.append(w_eff)
    H0_s.append(H0)
    Omega_m_0_s.append(Omega_m_0)
    Omega_nu_0_s.append(Omega_nu_0)
    Omega_q_0_s.append(Omega_q_0)
    m_bar_nu_0_s.append(m_bar_nu_0)


H0lcdm=0.000228173

#kappa = rho_m_0/(Omega_m_0*H0**2)


##eos_file = Base+'eos_data.txt'
##omega_file = Base+'omega_file.txt'

##eos_data_out = np.column_stack((a, w_q, w_nu, w_eff))
##omega_data_out = np.column_stack((a, Omega_q/units, Omega_nu/units, Omega_m/units))

##np.savetxt(eos_file, eos_data_out, fmt='%5.5e')  
##np.savetxt(omega_file, omega_data_out, fmt='%5.5e')

#axes1.plot(a_camb, H_camb/H_0_c, '-.', label='$H$ camb', color=collist[2], lw=2, ms=6)
#axes1.plot(a, H_a/H_0, '-', label='$H$', color=collist[3], lw=2, ms=6)

#plt.rc('text', usetex=True)
plt.rc('font', family='serif')

G = gridspec.GridSpec(4,4)
#choose image size, dpi and background color
fig=plt.figure(1, figsize=(20,12), dpi=80,facecolor='w')
#list of default colors for plot lines
collist=['b','g','r','c','Indigo','Olive','OrangeRed','SkyBlue', 'ForestGreen', 'DarkGoldenrod', 'GoldenRod', 'Plum', 'Crimson', 'SteelBlue', 'YellowGreen']
collist2=['Indigo','OrangeRed','SkyBlue', 'ForestGreen', 'DarkGoldenrod', 'GoldenRod', 'Plum', 'Crimson', 'SteelBlue', 'YellowGreen']
lslis=['-','--',':','-.','.']
# specify plot grid axes


indi=0

subs = [1., 2., 4., 6., 8.]

axes2 = fig.add_subplot(G[:2,:])

#axes2.plot(a_s[0],(Omegal*H0lcdm**2)/H_a_s[0]**2, '--', label='$\\Omega_{\\Lambda 0}=0.7$', color=collist[4], lw=2, ms=6)

indi=0
for a, Omega_m, Omega_nu, Omega_q  in zip(a_s, Omega_m_s, Omega_nu_s, Omega_q_s): 
    indj=0
    axes2.plot(a,Omega_m, lslis[indj], label=r'$\Omega_m$', color=collist2[indi], lw=2, ms=6)
    indj=1
    axes2.plot(a,Omega_nu, lslis[indj], label=r'$\Omega_{\nu}$', color=collist2[indi], lw=2, ms=6)
    indj=2
    axes2.plot(a,Omega_q, lslis[indj], label=r'$\Omega_{\phi}$', color=collist2[indi], lw=2, ms=6)
    indi+=1   

xmini,xmaxi,ymini,ymaxi = axes2.axis('tight')
axes2.axis(ymax=1.1, xmin=0.2, xmax=1.2, ymin=0)

axes2.legend(loc='best',markerscale=1.0,prop={'size':14},numpoints=3,handlelength=3)
axes2.grid(True,which="major",ls=":")
axes2.tick_params(length=6, width=1, labeltop=True, labelbottom=False)
axes2.set_ylabel(r"$\bar\Omega_{i} (a)$",size='x-large')
axes2.xaxis.set_label_position('top')
axes2.set_xlabel(r"$a$",size='x-large')


secondaxes1 = fig.add_subplot(G[2:3,:])

indi=0
for a, w_nu, w_q, Omega_q, Omega_nu in zip(a_s, w_nu_s, w_q_s, Omega_q_s, Omega_nu_s):

    linea=secondaxes1.plot(a, w_nu, lslis[indi], label=r'$w_{\nu}$', color=collist2[indi], lw=3)
    linea2=secondaxes1.plot(a, (w_q*Omega_q+w_nu*Omega_nu)/(Omega_nu+Omega_q), lslis[indi], label=r'$w_{eff}$', color=collist2[indi], lw=3)
    if (indi==2):
        seq=[2,4,7,4]
        linea[0].set_dashes(seq)
        linea2[0].set_dashes(seq)
    if (indi==4):
        seq=[5,2,10,5]
        linea[0].set_dashes(seq)
        linea2[0].set_dashes(seq)
    indi+=1
###    secondaxes1.plot(a, w_q, '--', label='$w_{\\phi}$', color=collist2[1], lw=2)

secondaxes1.axhline(y=0.33, ls='-.', lw=2, color='grey')
xmini,xmaxi,ymini,ymaxi = secondaxes1.axis('tight')
secondaxes1.axis(ymax=0.4, xmin=xmini*0.9, xmax=xmaxi*1.05, ymin=-1.1)



#xmini,xmaxi,ymini,ymaxi = secondaxes1.axis('tight')
#secondaxes1.axis(ymax=0.5, xmin=xmini, xmax=xmaxi*1.05, ymin=-1.1)

secondaxes1.legend(loc='center right',markerscale=1.0,prop={'size':16},numpoints=3,handlelength=3, ncol=3)
secondaxes1.grid(True,which="major",ls=":")
secondaxes1.tick_params(length=6, width=1)
secondaxes1.set_ylabel(r"$w (a)$",size='x-large')
#secondaxes1.xaxis.set_label_position('bottom')
#secondaxes1.set_xlabel(r"$a$",size='x-large')

print "weff plotted"

secondaxes2 = fig.add_subplot(G[3:4,:])
indi=0
for a, m_bar_nu in zip(a_s, m_bar_nu_s):

    secondaxes2.plot(a, m_bar_nu, lslis[indi], label=r'$m_{\nu}$', color=collist2[indi], lw=3)
    indi+=1

    
xmini,xmaxi,ymini,ymaxi = secondaxes2.axis('tight')
secondaxes2.axis(ymax=ymaxi*1.1, xmin=xmini*0.9, xmax=xmaxi*1.05, ymin=ymini)

secondaxes2.legend(loc='best',markerscale=1.0,prop={'size':15},numpoints=3,handlelength=3)
secondaxes2.grid(True,which="major",ls=":")
secondaxes2.tick_params(length=6, width=1)
secondaxes2.set_ylabel(r"$\bar m_{\nu} (a)$",size='x-large')
secondaxes2.xaxis.set_label_position('bottom')
secondaxes2.set_xlabel(r"$a$",size='x-large')


print "m_nu plotted"


#ratioArray1 = tools.takeRatio(datalist0[0],datalist1[0])
#ratioArray2 = tools.takeRatio(datalist0[0],datalist2[0])
#ratioArray3 = tools.takeRatio(datalist4[0],datalist0[0])


m_bar_nu_0 = "{:.2f}".format(m_bar_nu_0_s[0])
H_0="{:.2f}".format(H0_s[0])
Omega_m_0="{:.2f}".format(Omega_m_0_s[0])
Omega_nu_0="{:.3f}".format(Omega_nu_0_s[0])
Omega_q_0="{:.3f}".format(Omega_q_0_s[0])
zafinal_str="{:.3f}".format(z_redshift(afinal))

#q_0="{:.3f}".format(q_0)
#w_q_0="{:.3f}".format(w_q_0)

if plotend==0: 
    fig.suptitle('benchmark: '+simulabel+',  parameters z=0:  '+ r'$\Omega_\phi=$' + Omega_q_0 + r'$, \Omega_m=$'+Omega_m_0+r'$, \Omega_\nu=$'+Omega_nu_0+r'$, H_0=$'+H_0+r'$, m_{\nu0}=$'+m_bar_nu_0, fontsize=16)
else plotend==1:
    fig.suptitle('benchmark: '+simulabel+',  parameters z='+ zafinal_str +r'$\Omega_\phi=$'+Omega_q_0+r'$, \Omega_m=$'+Omega_m_0+r'$, \Omega_\nu=$'+Omega_nu_0+r'$, H=$'+H_0+r'$, m_{\nu}=$'+m_bar_nu_0, fontsize=16)



fig.savefig(imagefile,dpi=200)

plt.show()


###--possibly useful code--#

#axes1.text(0.02,0.1,'z=0',verticalalignment='top',horizontalalignment='left',transform=axes1.transAxes,fontsize=14,style='italic') 
#axes1.xaxis.set_minor_locator(ticker.LogLocator(subs=subs))
#axes1.xaxis.set_major_formatter(ticker.NullFormatter())
#axes1.xaxis.set_minor_formatter(ticker.FuncFormatter(ticks_format))
#axes1.xaxis.set_minor_formatter(ticker.LogFormatter(base=10.0, labelOnlyBase=False))
#minorLocator   = ticker.LogLocator(0.1,[0.01,0.02])
#axes1.xaxis.set_minor_locator(minorLocator)
#axes1.set_xticks(np.linspace(0.5,1.0,5))
#axes1.set_xticklabels(('0.1','0.2','0.4','0.5','0.6','0.7','0.8''0.9'))

###quantity=0 #0=general, 1=quintessence, 2=neutrinos
###delta_a = 0.01
###a_init = 0.02
###a_fin = 1.0
###a_values = np.arange(int(a_init*100)+1, int(a_fin*100), int(delta_a*100)) # or +1
###list_zerofill = [str(av).zfill(3) for av in a_values ]
###list_snaps = [aval.rstrip('0') for aval in list_zerofill]


