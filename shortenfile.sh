#!/bin/bash


folder=$1

nn=$2

if [ $# != 2 ]
then
    echo "Invalid arguments passed, need to pass folder and integer"
    exit 1

fi



echo "Folder name: $folder"
echo "Nth lines to keep: $nn"


cd $folder

echo `pwd`

#     `sed -n "1p;1~${nn}p"`  #this command prints the first line, then the first line again and then every nn-th line.
#   for printing really only every nn-th line, use the command as below, starting with 0

for fname in background_general.dat background_nu.dat background_q.dat mean_q.dat
  do
  
     filebody="${fname%.*}"

     extension="${fname#*.}"

     `sed -n "0~${nn}p" $fname > $filebody-shortened.$extension`

     echo "$filebody-shortened.$extension" written
     wc -l "$filebody-shortened.$extension" 

done

echo 4 Files shortened
