# coding: utf-8
import cProfile
import profile
get_ipython().magic(u'run -p -D profile-plot-time.out plot-SimusMore-gnq.py')
import pstats
pfile = pstats.Stats('profile-plot-time.out')
pfile.sort_stats('time').print_stats(15)
pfile.sort_stats('cumtime').print_stats(15)
get_ipython().magic(u'save how-to-profile.py 5-8 11 13 15')
