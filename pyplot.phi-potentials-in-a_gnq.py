import matplotlib.pyplot as plt
import numpy as np
import itertools
import sys
import copy
import matplotlib
import matplotlib.ticker as ticker
import matplotlib.gridspec as gridspec
import easygui as eg
from scipy import interpolate
import tools
import mycosmotools
from simusBaseDirs import *
from plotStyles import *

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

G = gridspec.GridSpec(4,4)
#choose image size, dpi and background color
fig=plt.figure(1, figsize=(20,12), dpi=80,facecolor='w')

#globalxmin=0.1
#globalxmax=1.1


shortlabel1 = 'mod1'
shortlabel2 = 'mod2'
shortlabel3 = 'mod3'


baselist=[Base1, Base2, Base3]
simulabel2="M2, "+r"$ \langle m_{\nu} \rangle (a_e) =0.07$"
simulabel3="M5, "+r"$ \langle m_{\nu} \rangle (a_e) =0.40 $"
simulablist = [simulabel1, simulabel2, simulabel3]
simulablistfn = ['M2', 'M5']
shortlablist = [shortlabel1, shortlabel2, shortlabel3]

#choose here using indices, which simulations to plot together or separately
pt=[1,2]

baselist=[v for indi,v in enumerate(baselist) if indi in pt]

simulablist=[v for indi,v in enumerate(simulablist) if indi in pt]
shortlablist=[v for indi,v in enumerate(shortlablist) if indi in pt]

a_times = np.array(range(4000,10500,500))
avail_times_full=[str(ii).zfill(6) for ii in a_times]
scalefacts= a_times/10000.0
scalefactstr=['a='+str(aa) for aa in scalefacts]
scalefactdict = dict(zip(avail_times_full,scalefacts))




avail_times_chosen0=avail_times_full[:]
avail_times_chosen1=avail_times_full[0:6]
avtimesHetero = [avail_times_chosen0, avail_times_chosen1]

def potfilesArray(avtims):
     
    potential_tot_files=['potspec_'+at+'_phi.dat' for at in avtims]
    potential_nu_files=['potspec_'+at+'_phi_nu.dat' for at in avtims]
    potential_m_files=['potspec_'+at+'_phi_m.dat' for at in avtims]
    potentials_files=[potential_tot_files, potential_nu_files, potential_m_files]
    return potentials_files

potential_files_hetero = [potfilesArray(avt) for avt in avtimesHetero]


# lof = list of files
potlabs=[r'$\Phi_t$',r'$\Phi_{\nu}$',r'$\Phi_m$']
#plot which potentials?
pwp=[1,2]

filepotlabs=['Phit','Phinu','Phim']
filepotlabs=[v for indi,v in enumerate(filepotlabs) if indi in pwp]


imagefile='../paper-simulation-plots/'+'Phi-GravPotSpec-InTime-'+'-and-'.join(simulablistfn)+'-2scales-'+"_".join(filepotlabs)+'-new'


list_of_files = pt
data_full = pt

for i,potfs in enumerate(potential_files_hetero):
    list_of_files[i] = [[baselist[i]+filename for filename in pot] for pot in potfs] 
    data_full[i] = [[np.loadtxt(filet)[:,:] for filet in filespm] for filespm in list_of_files[i]]        

#choose data set 



scale0 = data_full[0][pwp[0]][0][0][0]
print "extracting scale "+str(scale0)
simu0Phi0inscale0 = np.array([[ascale, potatscal[0][1]] for ascale, potatscal in zip(scalefacts, data_full[0][pwp[0]])])
scale1 = data_full[0][pwp[0]][0][6][0]
print "extracting scale "+str(scale1)
simu0Phi0inscale1 = np.array([[ascale, potatscal[6][1]] for ascale, potatscal in zip(scalefacts, data_full[0][pwp[0]])])

scale0 = data_full[0][pwp[1]][0][0][0]
print "extracting scale "+str(scale0)
simu0Phi1inscale0 = np.array([[ascale, potatscal[0][1]] for ascale, potatscal in zip(scalefacts, data_full[0][pwp[1]])])
scale1 = data_full[0][pwp[1]][0][6][0]
print "extracting scale "+str(scale1)
simu0Phi1inscale1 = np.array([[ascale, potatscal[6][1]] for ascale, potatscal in zip(scalefacts, data_full[0][pwp[1]])])

scale0 = data_full[1][pwp[0]][0][0][0]
print "extracting scale "+str(scale0)
simu1Phi0inscale0 = np.array([[ascale, potatscal[0][1]] for ascale, potatscal in zip(scalefacts, data_full[1][pwp[0]])])
scale1 = data_full[1][pwp[0]][0][6][0]
print "extracting scale "+str(scale1)
simu1Phi0inscale1 = np.array([[ascale, potatscal[6][1]] for ascale, potatscal in zip(scalefacts, data_full[1][pwp[0]])])

scale0 = data_full[1][pwp[1]][0][0][0]
print "extracting scale "+str(scale0)
simu1Phi1inscale0 = np.array([[ascale, potatscal[0][1]] for ascale, potatscal in zip(scalefacts, data_full[1][pwp[1]])])
scale1 = data_full[1][pwp[1]][0][6][0]
print "extracting scale "+str(scale1)
simu1Phi1inscale1 = np.array([[ascale, potatscal[6][1]] for ascale, potatscal in zip(scalefacts, data_full[1][pwp[1]])])


ftl = 30

lwid=4

secondaxes0 = fig.add_subplot(G[0:2,:])
secondaxes1 = fig.add_subplot(G[2:4,:], sharey=secondaxes0)

kArtist1 = plt.Line2D((0,1),(0,0), color='dimgray', linestyle=lslis[0], lw=lwid)
kArtist2 = plt.Line2D((0,1),(0,0), color='dimgray', linestyle=lslis[1], lw=lwid)

indi=0
plot_pot_lines=[]
lini=lslis[0]
col=colstand2[0] 
linp1, = secondaxes0.semilogy(simu0Phi0inscale0[:,0], simu0Phi0inscale0[:,1], lini, label=potlabs[pwp[0]], color=col, lw=lwid) 
lini=lslis[1]
linp2, = secondaxes0.semilogy(simu0Phi0inscale1[:,0], simu0Phi0inscale1[:,1], lini, color=col, lw=lwid) 
lini=lslis[0]
col=colstand2[1] 
linp3, = secondaxes0.semilogy(simu0Phi1inscale0[:,0], simu0Phi1inscale0[:,1], lini, label=potlabs[pwp[1]], color=col, lw=lwid) 
lini=lslis[1]
linp4, = secondaxes0.semilogy(simu0Phi1inscale1[:,0], simu0Phi1inscale1[:,1], lini, color=col, lw=lwid) 

plot_pot_lines.append([linp1, linp2, linp3, linp4])


#legend_pot1=secondaxes0.legend([kArtist1, kArtist2], ['k='+"{:.3f}".format(scale0),'k='+"{:.3f}".format(scale1)], loc='lower right', ncol=2, handlelength=3, prop={'size':ftl}) 

xmini,xmaxi,ymini,ymaxi = secondaxes0.axis('tight')

xmaxa = 1.05
ftt = 40

secondaxes0.axis(ymax=ymaxi*1.2, xmin=xmini, xmax=xmaxi*1.02, ymin=0.8*ymini)

#secondaxes0.add_artist(legend_pot1)
#secondaxes0.legend(loc='upper right',markerscale=1.0,prop={'size':ftl},numpoints=3,handlelength=3,ncol=2)
secondaxes0.grid(True,which="major",ls=":")
secondaxes0.tick_params(length=10, width=1.5, labelsize=ftl, labelleft='on', labelright='off', which='major')
secondaxes0.tick_params(length=4, width=1, which='minor')
#secondaxes0.set_ylabel(r"Gravitational potential $\bar \Phi(k,a)$",fontsize=ftl)
secondaxes0.xaxis.set_ticks_position('top')

secondaxes0.set_xlabel(r"$\mathbf{a}$",fontsize=ftt)
secondaxes0.xaxis.set_label_position('top')
secondaxes0.xaxis.labelpad = 15

secondaxes0.annotate( simulablist[0], xy=(0.938, 2*10**-7), xycoords='data', ha="center", va="center", size=ftl, bbox=bbox_props)

print "phi plotted "+simulablist[0]

indi=0
plot_pot_lines=[]
lini=lslis[0]
col=colstand2[0] 
linp1, = secondaxes1.semilogy(simu1Phi0inscale0[:,0], simu1Phi0inscale0[:,1], lini, label=potlabs[pwp[0]], color=col, lw=lwid) 
lini=lslis[1]
linp2, = secondaxes1.semilogy(simu1Phi0inscale1[:,0], simu1Phi0inscale1[:,1], lini, color=col, lw=lwid) 
lini=lslis[0]
col=colstand2[1] 
linp3, = secondaxes1.semilogy(simu1Phi1inscale0[:,0], simu1Phi1inscale0[:,1], lini, label=potlabs[pwp[1]], color=col, lw=lwid) 
lini=lslis[1]
linp4, = secondaxes1.semilogy(simu1Phi1inscale1[:,0], simu1Phi1inscale1[:,1], lini, color=col, lw=lwid) 

plot_pot_lines.append([linp1, linp2, linp3, linp4])

legend_pot1=secondaxes1.legend([kArtist1, kArtist2], ['k='+"{:.3f}".format(scale0),'k='+"{:.3f}".format(scale1)], loc='lower right', ncol=2, handlelength=3,prop={'size':ftl}) 
xmini,xmaxi,ymini,ymaxi = secondaxes1.axis('tight')
secondaxes1.axis(ymax=ymaxi*1.2, xmin=xmini, xmax=xmaxi*1.02, ymin=0.8*ymini)
secondaxes1.add_artist(legend_pot1)
legg = secondaxes1.legend(loc='lower left',markerscale=1.0,prop={'size':ftl},numpoints=3,handlelength=3)
secondaxes1.grid(True,which="major",ls=":")
secondaxes1.tick_params(length=10, width=1.5, labelsize=ftl, labelleft='on', labelright='off', which='major')
secondaxes1.tick_params(length=4, width=1, which='minor')
#secondaxes1.set_ylabel(r"Gravitational potential $\bar \Phi(k,a)$",fontsize=ftl)
secondaxes1.set_xlabel(r"$\mathbf{a}$",fontsize=ftt)
secondaxes1.xaxis.set_ticks_position('both')
secondaxes1.yaxis.set_ticks_position('both')

secondaxes1.xaxis.labelpad = 1
secondaxes1.annotate(simulablist[1], xy=(0.627, 2*10**-7), xycoords='data', ha="center", va="center", size=ftl, bbox=bbox_props)


for l in legg.legendHandles:            
    l.set_linewidth(10)


plt.subplots_adjust(hspace=0.09)



yy=plt.ylabel(r"Gravitational Potential $\;\bar \Phi(k)$",fontsize=ftt)
yy.set_position((yy.get_position()[0],1))


print "phi plotted "+simulablist[1]



plt.show()

boolplot=eg.ynbox(msg="Save plots?")

if (boolplot==True):
    fig.savefig(imagefile+".png",dpi=400, bbox_inches='tight')
    print "saving png"
    fig.savefig(imagefile+".pdf",dpi=400, bbox_inches='tight')
    print "saving pdf"
    fig.savefig(imagefile+".eps",format="eps", dpi=1000, bbox_inches='tight')
    print "saving eps"
else:
    print "no plots saved"






print 'finished'

plt.close()
