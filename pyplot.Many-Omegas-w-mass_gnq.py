import matplotlib.pyplot as plt
import numpy as np
import sys
import copy
import matplotlib
import matplotlib.ticker as ticker
import matplotlib.gridspec as gridspec
from scipy import interpolate
import itertools
from tools import *
from mycosmotools import *
from simusBaseDirs import *

baselist=[Base1, Base2, Base3, Base4, Base5, Base6]

simulablist = [simulabel1, simulabel2, simulabel3, simulabel4, simulabel5, simulabel6]

#choose here using indices, which simulations to plot together or separately
toplot=[2,4]

baselist=[v for indi,v in enumerate(baselist) if indi in toplot]

simulablist=[v for indi,v in enumerate(simulablist) if indi in toplot]

# choose plotend=0 to write quantities until z=0, or plotend=1 to write until last a-value of files or a float like plotend=0.55 to plot until that scale factor
plotend=[1,1]


imagefile='../simulation-plots/'+'-and-'.join(simulablist)+'-comparison-new'

#choose for the displayed cosmo quantities on the title, from which simulation they should be shown. In the order of simulablist and baselist.

# lof = list of files
background_from = ['background_general-shortened.dat','background_q-shortened.dat','background_nu-shortened.dat']

list_of_back_files = [[base+filename for filename in background_from] for base in baselist] 

#choose data set 
data_full = [[np.loadtxt(bckfile)[:,:] for bckfile in bckfiles] for bckfiles in list_of_back_files]





H0units=1.0/299792

###-------------------------##
a_s=[]
H_a_s=[]
rho_m_s=[]
rho_nu_s=[]
rho_tot_s=[]
p_nu_s=[]
m_bar_nu_s=[]
w_nu_s=[]
qui_s=[]
qui_dot_s=[]
rho_q_s=[]
p_q_s=[]
w_q_s=[]
Omega_m_s=[]
Omega_nu_s=[]
Omega_q_s=[]
w_eff_s=[]
H0_s=[]
units_s=[]
rho_m_0_s=[]
Omega_m_0_s=[]
Omega_nu_0_s=[]
Omega_q_0_s=[]
q_0_s=[]
w_q_0_s=[]
m_bar_nu_0_s=[]
a_final_s=[]

for j, databck in enumerate(data_full):

    a, H_a, rho_m = extract_columns(databck[0], quant=0)
    a, rho_nu, p_nu, m_bar_nu = extract_columns(databck[2], quant=2)
    a, qui, qui_dot = extract_columns(databck[1], quant=1)
    
    rho_q, p_q, w_q = w_eq_of_state(qui, qui_dot)

    rho_total_simu = rho_total(rho_m, rho_nu, rho_q) 
    
    Omega_m = Omega_calc(rho_m, rho_total_simu)

    Omega_nu = Omega_calc(rho_nu, rho_total_simu)

    Omega_q = Omega_calc(rho_q, rho_total_simu)

    w_nu = p_nu/rho_nu
    w_eff = (w_q*Omega_q+w_nu*Omega_nu)/(Omega_nu+Omega_q)

    if plotend[j]==1 :
        afinal=a[-1]
    elif plotend[j]==0 : 
        afinal=1.0
    else :
        afinal=plotend[j]

    H0 = float(interpolate.interp1d(a, H_a)(afinal))/H0units
    Omega_m_0 = float(interpolate.interp1d(a, Omega_m)(afinal))
    Omega_nu_0 = float(interpolate.interp1d(a, Omega_nu)(afinal))
    Omega_q_0 = float(interpolate.interp1d(a, Omega_q)(afinal))
    m_bar_nu_0 = float(interpolate.interp1d(a, m_bar_nu)(afinal)) 
    
    a_s.append(a)
    H_a_s.append(H_a)
    rho_m_s.append(rho_m)
    rho_nu_s.append(rho_nu)
    p_nu_s.append(p_nu)
    m_bar_nu_s.append(m_bar_nu)
    w_nu_s.append(w_nu)
    qui_s.append(qui)
    qui_dot_s.append(qui_dot)
    rho_q_s.append(rho_q)
    p_q_s.append(p_q)
    w_q_s.append(w_q)
    Omega_m_s.append(Omega_m)
    Omega_nu_s.append(Omega_nu)
    Omega_q_s.append(Omega_q)
    w_eff_s.append(w_eff)
    H0_s.append(H0)
    Omega_m_0_s.append(Omega_m_0)
    Omega_nu_0_s.append(Omega_nu_0)
    Omega_q_0_s.append(Omega_q_0)
    m_bar_nu_0_s.append(m_bar_nu_0)
    a_final_s.append(afinal)


H0lcdm=0.000228173

#plt.rc('text', usetex=True)
plt.rc('font', family='serif')

G = gridspec.GridSpec(4,4)
#choose image size, dpi and background color
fig=plt.figure(1, figsize=(20,12), dpi=80,facecolor='w')

#list of default colors for plot lines
collist=['b','g','r','c','Indigo','Olive','OrangeRed','SkyBlue', 'ForestGreen', 'DarkGoldenrod', 'GoldenRod', 'Plum', 'Crimson', 'SteelBlue', 'YellowGreen']
collist2=['Indigo','OrangeRed','SkyBlue', 'ForestGreen', 'DarkGoldenrod', 'GoldenRod', 'Plum', 'Crimson', 'SteelBlue', 'YellowGreen']
lslis=['-','--',':','-.','.']
lslis2=lslis[::-1]
indi=0
subs = [1., 2., 4., 6., 8.]

# specify plot grid axes
axes2 = fig.add_subplot(G[:2,:])

#axes2.plot(a_s[0],(Omegal*H0lcdm**2)/H_a_s[0]**2, '--', label='$\\Omega_{\\Lambda 0}=0.7$', color=collist[4], lw=2, ms=6)

globalxmin=0.1
globalxmax=1.1


indi=0
cycol=itertools.cycle(collist2)
cylin=itertools.cycle(lslis)
plot_om_lines=[]
for a, Omega_m, Omega_nu, Omega_q  in zip(a_s, Omega_m_s, Omega_nu_s, Omega_q_s): 
    collin=next(cycol)
    lin_om1, = axes2.plot(a,Omega_m, lslis[0], label=r'$\Omega_m$', color=collin, lw=2, ms=6)
    lin_om2, = axes2.plot(a,Omega_nu, lslis[1], label=r'$\Omega_{\nu}$', color=collin, lw=2, ms=6)
    lin_om3, = axes2.plot(a,Omega_q, lslis[2], label=r'$\Omega_{\phi}$', color=collin, lw=2, ms=6)
    plot_om_lines.append([lin_om1, lin_om2, lin_om3])

xmini,xmaxi,ymini,ymaxi = axes2.axis('tight')
axes2.axis(ymax=1.1, xmin=globalxmin, xmax=globalxmax, ymin=0)

legend_om1=axes2.legend(plot_om_lines[0], [r'$\Omega_m$', r'$\Omega_{\nu}$', r'$\Omega_{\phi}$'], loc='center left', ncol=3)
axes2.legend(loc='best',markerscale=1.0,prop={'size':14},numpoints=3,handlelength=3)
axes2.legend([li[0] for li in plot_om_lines], simulablist, loc='upper right', ncol=len(simulablist))
plt.gca().add_artist(legend_om1)
axes2.grid(True,which="major",ls=":")

axes2.tick_params(length=6, width=1, labeltop=True, labelbottom=False)
axes2.set_ylabel(r"$\bar\Omega_{i} (a)$",size='x-large')
axes2.xaxis.set_label_position('bottom')
axes2.set_xlabel(r"$a$",size='x-large')

print "Omegas plotted"


secondaxes1 = fig.add_subplot(G[2:3,:])
cycol=itertools.cycle(collist2)
indi=0
plot_w_lines=[]
for a, w_nu, w_q, Omega_q, Omega_nu in zip(a_s, w_nu_s, w_q_s, Omega_q_s, Omega_nu_s):
    collin=next(cycol)
    lin_w1, = secondaxes1.plot(a, w_nu, lslis[0], label=r'$w_{\nu}$', color=collin, lw=3)
    #collin=next(cycol)
    lin_w2, = secondaxes1.plot(a, (w_q*Omega_q+w_nu*Omega_nu)/(Omega_nu+Omega_q), lslis2[1], label=r'$w_{eff}$', color=collin, lw=3)
    plot_w_lines.append([lin_w1, lin_w2])

secondaxes1.axhline(y=0.33, ls='-.', lw=2, color='grey')
xmini,xmaxi,ymini,ymaxi = secondaxes1.axis('tight')
legend_w1=secondaxes1.legend(plot_w_lines[0], [r'$w_{\nu}$', r'$w_{eff}$'], loc='lower left', ncol=2)
secondaxes1.legend([li[0] for li in plot_w_lines], simulablist, loc='center right', ncol=len(simulablist))
#markerscale=1.0,prop={'size':15},numpoints=3,handlelength=3
plt.gca().add_artist(legend_w1)
secondaxes1.axis(ymax=0.4, xmin=globalxmin, xmax=globalxmax, ymin=-1.1)
#secondaxes1.legend(loc='center right',markerscale=1.0,prop={'size':16},numpoints=3,handlelength=3, ncol=3)


secondaxes1.grid(True,which="major",ls=":")
secondaxes1.tick_params(length=6, width=1)
secondaxes1.set_ylabel(r"$w (a)$",size='x-large')

###    secondaxes1.plot(a, w_q, '--', label='$w_{\\phi}$', color=collist2[1], lw=2)
#xmini,xmaxi,ymini,ymaxi = secondaxes1.axis('tight')
#secondaxes1.axis(ymax=0.5, xmin=xmini, xmax=xmaxi*1.05, ymin=-1.1)
#secondaxes1.xaxis.set_label_position('bottom')
#secondaxes1.set_xlabel(r"$a$",size='x-large')

print "weff plotted"

secondaxes2 = fig.add_subplot(G[3:4,:])
indi=0
plot_m_lines=[]
for a, m_bar_nu in zip(a_s, m_bar_nu_s):

    linm1, = secondaxes2.plot(a, m_bar_nu, lslis[1], label=r"$\bar m_{\nu}$", color=collist2[indi], lw=3)
    indi+=1
    plot_m_lines.append([linm1])

    
xmini,xmaxi,ymini,ymaxi = secondaxes2.axis('tight')
secondaxes2.axis(ymax=ymaxi*1.1, xmin=globalxmin, xmax=globalxmax, ymin=ymini)
legend_m1=secondaxes2.legend(plot_m_lines[0], [r"$\bar m_{\nu}$"],loc='upper right')
secondaxes2.legend([li[0] for li in plot_m_lines], simulablist, loc='upper left')
#markerscale=1.0,prop={'size':15},numpoints=3,handlelength=3
plt.gca().add_artist(legend_m1)
secondaxes2.grid(True,which="major",ls=":")
secondaxes2.tick_params(length=6, width=1)
secondaxes2.set_ylabel(r"$\bar m_{\nu} (a)$",size='x-large')
secondaxes2.xaxis.set_label_position('bottom')
secondaxes2.set_xlabel(r"$a$",size='x-large')


print "m_nu plotted"


#ratioArray1 = tools.takeRatio(datalist0[0],datalist1[0])
#ratioArray2 = tools.takeRatio(datalist0[0],datalist2[0])
#ratioArray3 = tools.takeRatio(datalist4[0],datalist0[0])

print "Writing parameters of "+'-and-'.join(simulablist)+" on plot title"

m_bar_nu_0_sim = ["{:.2f}".format(quant) for quant in m_bar_nu_0_s]
H_0_sim = ["{:.2f}".format(quant) for quant in H0_s]
Omega_m_0_sim= ["{:.2f}".format(quant) for quant in Omega_m_0_s]
Omega_nu_0_sim= ["{:.2f}".format(quant) for quant in Omega_nu_0_s]
Omega_q_0_sim = ["{:.2f}".format(quant) for quant in Omega_q_0_s]
afinal_str_sim = ["{:.2f}".format(quant) for quant in a_final_s]

#q_0="{:.3f}".format(q_0)
#w_q_0="{:.3f}".format(w_q_0)

parameters_string=[]
for i in range(len(simulablist)):
    parameters_string.append('model: '+simulablist[i]+',  parameters at a='+ afinal_str_sim[i] +": "+r'$\;\Omega_\phi=$'+Omega_q_0_sim[i]+r'$,\; \Omega_m=$'+Omega_m_0_sim[i]+r'$,\; \Omega_\nu=$'+Omega_nu_0_sim[i]+r'$,\; H=$'+H_0_sim[i]+r'$,\; m_{\nu}=$'+m_bar_nu_0_sim[i])

subtitle="\n".join(parameters_string)

fig.suptitle(subtitle, fontsize=16)


fig.savefig(imagefile,dpi=300)

plt.show()



