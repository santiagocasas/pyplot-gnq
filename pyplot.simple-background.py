import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import matplotlib.ticker as plticker
import itertools
import sys
import copy
import matplotlib
import matplotlib.ticker as ticker
import matplotlib.gridspec as gridspec
import easygui as eg
from scipy import interpolate
import tools
import mycosmotools
from simusBaseDirs import *
from plotStyles import *
import glob

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

G = gridspec.GridSpec(4,4)
#choose image size, dpi and background color
fig=plt.figure(1, figsize=(20,12), dpi=80,facecolor='w')

#globalxmin=0.1
#globalxmax=1.1

Base = '../backreaction/data-tables-for-plotting/'

shortlabel = 'mod2'
simulab = 'model M2'    #simulabel2
model = 'model2'
quantities = ['OmegaFluidQNu', 'OmegaNu']
sources = ['CAMB','gnq']
sources = sources[::-1]
formattxt = '.txt'

qlabels = [r"$\Omega_{\nu+\phi}$",r"$\Omega_\nu$"]
#soulabels = ['nuCAMB','gnqsim']
soulabels = ['linear','N-body']
soulabels = soulabels[::-1]

sl = len(sources)
ql = len(quantities)


dataqu = np.empty([sl,ql], dtype=object)
filequ = np.empty([sl,ql], dtype=object)

#filequ = [['1','b'],['a','c']]

for si,s in enumerate(sources):
    for qi, q in enumerate(quantities):
        filequ[si][qi] = (glob.glob(Base+s+'-'+model+'*'+'-'+q+formattxt))[0]
        dataqu[si,qi] = np.loadtxt(filequ[si][qi])[:,:]


print "hola"

imagefile='../paper-simulation-plots/'+'Background-'+model+'-'+'-And-'.join(sources)+'_'+'-and-'.join(quantities)+'-v1'


globalxmin=0.1
globalxmax=1.1

ftl = 40 #fontsizeleg
ftt = 35 #fontsize tick labs

lwid=4

axes0 = fig.add_subplot(G[:,:])


plot_pot_lines=[]

for i in range(sl):
    lini=lslis[i]
    indi = 0
    for j in range(ql): 
        col=colstand2[::-1][indi] 
        linp1, = axes0.plot(dataqu[i,j][:,0], dataqu[i,j][:,1], lini, label=qlabels[j], color=col, lw=lwid)
        indi = indi + 1 
      
        plot_pot_lines.append([linp1])

sArtist1 = plt.Line2D((0,1),(0,0), color='dimgray', linestyle=lslis[0], lw=lwid)
sArtist2 = plt.Line2D((0,1),(0,0), color='dimgray', linestyle=lslis[1], lw=lwid)

legend_pot11=axes0.legend([sArtist1, sArtist2], [soulabels[0], soulabels[1]], loc='upper left', ncol=1,prop={'size':ftl}, handlelength=3) 

xmini,xmaxi,ymini,ymaxi = axes0.axis('tight')
print xmini, xmaxi
axes0.axis(ymax=1.0, xmin=0.15, xmax=1.05, ymin=0.0) 

axes0.add_artist(legend_pot11)

handless, labelss = axes0.get_legend_handles_labels()
#pp1 = mpatches.Patch(color=collist3[1], alpha=0.4, linewidth=0)

legg = axes0.legend([li[0] for li in plot_pot_lines],qlabels,loc='upper right',markerscale=1.0,prop={'size':ftl},numpoints=3,handlelength=2)

for l in legg.legendHandles:            
    l.set_linewidth(10)

axes0.grid(True,which="major",ls=":")
axes0.tick_params(length=6, width=1, labelsize=ftt, labelright='on', labelleft='off')
#axes0.xaxis.tick_top()
axes0.set_ylabel(r"Background  $\Omega_{i}$",fontsize=ftl)
axes0.set_xlabel(r"scale factor $a$",fontsize=ftl)
axes0.xaxis.set_label_position('bottom')
#axes0.tick_params(axis='x', pad=10)

#start, end = axes0.get_xlim()
#axes0.xaxis.set_ticks(np.arange(start, end, 0.1))
#axes0.xaxis.set_major_formatter(ticker.FormatStrFormatter('%0.1f'))


loc = plticker.MultipleLocator(base=0.1) # this locator puts ticks at regular intervals
axes0.yaxis.set_major_locator(loc)




axes0.annotate(simulab, xy=(0.5, 0.92), xycoords='axes fraction', ha="center", va="center", size=40, bbox=bbox_props)

##axes0.set_title("Model "+simulablist[0])

print simulab+" plotted"


plt.show()

boolplot=eg.ynbox(msg="Save plots?")

if (boolplot==True):
    fig.savefig(imagefile+".png",dpi=400, bbox_inches='tight')
    print "saving png"
    fig.savefig(imagefile+".pdf",dpi=400, bbox_inches='tight')
    print "saving pdf"
    fig.savefig(imagefile+".eps",format="eps", dpi=1000, bbox_inches='tight')
    print "saving eps"
else:
    print "no plots saved"


plt.close()

print "finished"
