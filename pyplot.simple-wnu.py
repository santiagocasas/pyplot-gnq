import matplotlib.pyplot as plt
import numpy as np
import itertools
import sys
import copy
import matplotlib
import matplotlib.ticker as ticker
import matplotlib.gridspec as gridspec
import easygui as eg
from scipy import interpolate
import tools
import mycosmotools
from simusBaseDirs import *
from plotStyles import *
import glob

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

G = gridspec.GridSpec(4,4)
#choose image size, dpi and background color
fig=plt.figure(1, figsize=(20,12), dpi=80,facecolor='w')

#globalxmin=0.1
#globalxmax=1.1

Base = '../backreaction/data-tables-for-plotting/'

shortlabel = 'mod2'
simulab = "model M2"+"\n"+r"$ \langle m_{\nu} \rangle (a_l) = 0.164$"
model = 'model2'
quantities = ['wnu', 'wqnu']
sources = ['CAMB','gnq']
sources = sources[::-1]
formattxt = '.txt'

qlabels = [r"$w_\nu$", r"$w_{\phi+\nu}$"]
#soulabels = ['nuCAMB','gnqsim']
soulabels = ['linear','N-body']
soulabels = soulabels[::-1]

sl = len(sources)
ql = len(quantities)


dataqu = np.empty([sl,ql], dtype=object)
#filequ = np.empty([sl,ql],dtype=str)

filequ = [['1','b'],['a','c']]

for si,s in enumerate(sources):
    for qi, q in enumerate(quantities):
        filequ[si][qi] = (glob.glob(Base+s+'-'+model+'*'+'-'+q+formattxt))[0]
        dataqu[si,qi] = np.loadtxt(filequ[si][qi])[:,:]



imagefile='../paper-simulation-plots/'+'Background-'+model+'-'+'-And-'.join(sources)+'_'+'-and-'.join(quantities)+'-v1'


print "printing image file: "+imagefile

#globalxmin=0.1
#globalxmax=1.1

ftl = 40 #fontsizeleg
ftt = 35 #fontsize tick labs

lwid=4

axes0 = fig.add_subplot(G[:,:])



plot_pot_lines=[]

for i in range(sl):
    lini=lslis[i]
    indi = 0
    for j in range(ql): 
        col=collist3[2+indi] 
        linp1, = axes0.plot(dataqu[i,j][:,0], dataqu[i,j][:,1], lini, label=qlabels[j], color=col, lw=lwid)
        indi = indi + 1 
      
        plot_pot_lines.append([linp1])

sArtist1 = plt.Line2D((0,1),(0,0), color='dimgray', linestyle=lslis[0], lw=lwid)
sArtist2 = plt.Line2D((0,1),(0,0), color='dimgray', linestyle=lslis[1], lw=lwid)

legend_pot11=axes0.legend([sArtist1, sArtist2], [soulabels[0], soulabels[1]], loc='upper left', ncol=2,prop={'size':ftl}, handlelength=3,borderaxespad=0.1) 

xmini,xmaxi,ymini,ymaxi = axes0.axis('tight')
print xmini, xmaxi
axes0.axis(ymax=0.5, xmin=0.15, xmax=1.05, ymin=-1.1) 

axes0.add_artist(legend_pot11)
legg=axes0.legend([li[0] for li in plot_pot_lines],qlabels,loc='center right',markerscale=1.0,prop={'size':ftl},numpoints=3,handlelength=2)

axes0.grid(True,which="major",ls=":")
axes0.tick_params(length=6, width=1, labelsize=ftt, labelright='off', labelleft='on')
#axes0.xaxis.tick_top()

for l in legg.legendHandles:            
    l.set_linewidth(10)

axes0.set_ylabel(r"Equation of state  $w_{i}$",fontsize=ftl)
axes0.set_xlabel(r"scale factor $a$",fontsize=ftl)
axes0.xaxis.set_label_position('bottom')
#axes0.tick_params(axis='x', pad=10)

axes0.annotate( simulab , xy=(0.86, 0.32), xycoords='axes fraction', ha="center", va="center", size=35, bbox=bbox_props)

##axes0.set_title("Model "+simulablist[0])

print simulab+" plotted"


plt.show()

boolplot=eg.ynbox(msg="Save plots?")

saveplots(boolplot,imagefile,fig)


plt.close()

print "finished"
