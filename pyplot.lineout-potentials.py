import matplotlib.pyplot as plt
import numpy as np
import itertools
import sys
import copy
import matplotlib
import matplotlib.ticker as ticker
import matplotlib.gridspec as gridspec
from scipy import interpolate
import tools
import mycosmotools
from simusBaseDirs import *
import easygui as eg
from plotStyles import *


shortlabel1 = 'mod1'

base = "../visit-plots/new-plots-paper/"
filebase="model2-"
fileend="-a0p75-lineout-diag.curve"
filequs = ["numbnu", "phinu", "psinu","phim"]
files = [filebase+fi+fileend for fi in filequs]


quslabs=[r'$ \mathbf{n_{\nu}(x)/\bar{n}_\nu - 1}$',r'$\mathbf{ -\Phi_{\nu} }$',r'$\Psi_{\nu}$',r'$\Phi_m$']
filepotlabs=['numbnu','Phinu','Psinu','Phim'] 

#plot which potentials?
pwp=[0,1]


filepotlabs=[v for indi,v in enumerate(filepotlabs) if indi in pwp]
whichfiles = [v for indi,v in enumerate(files) if indi in pwp]
quslabs = [v for indi,v in enumerate(quslabs) if indi in pwp]


imagefile='../paper-simulation-plots/'+'Lineout-Diagonal-'+'-at-'+'a0p75'+'-'+"_".join(filepotlabs)

list_of_files = [base+filename for filename in whichfiles] 

#choose data set 
data_full = [np.loadtxt(fi)[:,:] for fi in list_of_files]        

G = gridspec.GridSpec(4,4)

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

#choose image size, dpi and background color

fig=plt.figure(1, figsize=(20,12), dpi=100,facecolor='w')

indi=0

subs = [1., 2., 4., 6., 8.]

globalxmin=0.1
globalxmax=1.1

ftl=40 #fontsizeleg
lwid=4

ax0 = fig.add_subplot(G[:,:])

lini=lslis[0]
col='black' 

firstqu = data_full[0]

ax0.plot(firstqu[:,0], firstqu[:,1]-1.0, lini, label=filepotlabs[0], color=col, lw=lwid) 


#xmini,xmaxi,ymini,ymaxi = ax0.axis('tight')
#print "tight axes ranges: "
#print xmini, xmaxi
#print ymini, ymaxi

#ax0.axis(ymax=10.0**-4, xmin=0.01, xmax=1.10, ymin=10.0**-9) 
ax0.grid(True,which="major",ls=":")
ax0.tick_params(length=6, width=1, labelsize=ftl)

ax0.set_xlabel(r'Distance in Mpc/h', fontsize=ftl)
ax0.set_ylabel(quslabs[0], color=col, fontsize=ftl)

for t0 in ax0.get_yticklabels():
    t0.set_color(col)

ax0.yaxis.get_offset_text().set_color(col)
ax0.yaxis.get_offset_text().set_size(ftl)
#ax0.xaxis.tick_top()
#ax0.tick_params(axis='x', pad=10)
#ax0.yaxis.set_label_position('right')

#ax0.annotate(simulablist[0], xy=(0.05, 0.5), xycoords='axes fraction', ha="center", va="center", size=20, bbox=bbox_props)

##ax0.set_title("Model "+simulablist[0])
##ax0.xaxis.set_label_position('bottom')

print filepotlabs[0]+" plotted"

########## plot 1 

ax1 = ax0.twinx()

lini=lslis[1]
col = 'royalblue'

secondqu = data_full[1]

ax1.plot(secondqu[:,0], -1*secondqu[:,1], lini, label=filepotlabs[1], color=col, lw=lwid) 


#xmini,xmaxi,ymini,ymaxi = ax1.axis('tight')
#print "tight axes ranges: "
#print xmini, #xmaxi
#print ymini, ymaxi

#ax1.axis(ymax=10.0**-4, xmin=0.01, xmax=1.10, ymin=10.0**-9) 
#ax1.grid(True,which="major",ls=":")
#ax1.tick_params(length=6, width=1, labelsize=ftl)

#ax1.set_xlabel(r'Distance in Mpc/h', fontsize=ftl)
ax1.set_ylabel(quslabs[1], color=col, fontsize=ftl)
for t1 in ax1.get_yticklabels():
    t1.set_color(col)


ax1.tick_params(length=6, width=1, labelsize=ftl)
#ax1.xaxis.tick_top()
#ax1.tick_params(axis='x', pad=10)
#ax1.yaxis.set_label_position('right')

#ax1.annotate(simulablist[0], xy=(0.05, 0.5), xycoords='axes fraction', ha="center", va="center", size=20, bbox=bbox_props)

##ax1.set_title("Model "+simulablist[0])
##ax1.xaxis.set_label_position('bottom')
ax1.yaxis.get_offset_text().set_color(col)
ax1.yaxis.get_offset_text().set_fontsize(ftl)


#yy=plt.ylabel(r"Gravitational Potential $\bar \Phi(k)$",fontsize=ftl)
#yy.set_position((yy.get_position()[0],1))

#plt.subplots_adjust(hspace=0.07)

print filepotlabs[1]+" plotted"


plt.show()

boolplot=eg.ynbox(msg="Save plots?")

if (boolplot==True):
    fig.savefig(imagefile+".png",dpi=400, bbox_inches='tight')
    print "saving png"
    fig.savefig(imagefile+".pdf",dpi=400, bbox_inches='tight')
    print "saving pdf"
    fig.savefig(imagefile+".eps",format="eps", dpi=1000, bbox_inches='tight')
    print "saving ps"
else:
    print "no plots saved"


plt.close()

print "finished"


