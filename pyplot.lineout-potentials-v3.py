import matplotlib.pyplot as plt
import numpy as np
import itertools
import sys
import copy
import matplotlib
import matplotlib.ticker as ticker
import matplotlib.gridspec as gridspec
from scipy import interpolate
import tools
import mycosmotools
from simusBaseDirs import *
import easygui as eg
from plotStyles import *


shortlabel1 = 'mod1'

base = "../visit-plots/"
filebase=""
fileend="-a0p75-lineout-0-64.txt.curve"
filequs = ["fieldnu","numbnu", "Phinu"]
files = [filebase+fi+fileend for fi in filequs]


quslabs=[r'$ \mathbf{\delta_{\nu}(x)}$',r'$ \mathbf{\delta n_{\nu}(x)}$',r'$\mathbf{ -\Phi_{\nu} }$']
filepotlabs=['deltanu','numbnu','Phinu'] 

#plot which potentials?
pwp=[0,1,2]

gridconvfactmpc = 300.0/0.7/64.0


filepotlabs=[v for indi,v in enumerate(filepotlabs) if indi in pwp]
whichfiles = [v for indi,v in enumerate(files) if indi in pwp]
quslabs = [v for indi,v in enumerate(quslabs) if indi in pwp]


imagefile='../paper-simulation-plots/'+'Lineout-Diagonal-'+'-at-'+'a0p75'+'-'+"_".join(filepotlabs)

list_of_files = [base+filename for filename in whichfiles] 

#choose data set 
data_full = [np.loadtxt(fi)[:,:] for fi in list_of_files]        

G = gridspec.GridSpec(4,4)

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

#choose image size, dpi and background color

fig=plt.figure(1, figsize=(20,12), dpi=100,facecolor='w')

indi=0

subs = [1., 2., 4., 6., 8.]

globalxmin=0.1
globalxmax=1.1

ftl=35 #fontsizeleg
lwid=4

ax0 = fig.add_subplot(G[:,:])

########## plot 1 

lini=lslis[0]
col='black' 

firstqu = data_full[0]

ax0.plot(gridconvfactmpc*firstqu[:,0], firstqu[:,1], lini, label=quslabs[0], color=col, lw=lwid) 

ax0.grid(True,which="major",ls=":")
ax0.tick_params(length=6, width=1, labelsize=ftl)
ax0.set_xlabel(r'Distance in Mpc/h', fontsize=ftl)
ax0.set_ylabel(" , ".join(quslabs[0:2]), color=col, fontsize=ftl)

for t0 in ax0.get_yticklabels():
    t0.set_color(col)

ax0.yaxis.get_offset_text().set_color(col)
ax0.yaxis.get_offset_text().set_size(ftl)

print filepotlabs[0]+" plotted"

########## plot 1 


lini=lslis[1]
col = 'darkorange'

secondqu = data_full[1]

ax0.plot(gridconvfactmpc*secondqu[:,0], secondqu[:,1]-1.0, lini, label=quslabs[1], color=col, lw=lwid) 

#ax1.set_ylabel(quslabs[1], color=col, fontsize=ftl)
#for t1 in ax1.get_yticklabels():
#    t1.set_color(col)

ax0.legend(loc="upper right", fontsize=ftl)

print filepotlabs[1]+" plotted"

########## plot 2 

ax2 = ax0.twinx()

lini=lslis[2]
col = 'royalblue'

thirdqu = data_full[2]
custdash = [16,8,4,8]

ax2.plot(gridconvfactmpc*thirdqu[:,0], -1.0*thirdqu[:,1], '-', dashes=custdash, label=quslabs[2], color=col, lw=lwid) 

ax2.set_ylabel(quslabs[2], color=col, fontsize=ftl)
for t1 in ax2.get_yticklabels():
    t1.set_color(col)


xmini,xmaxi,ymini,ymaxi = ax2.axis('tight')
ax2.axis(ymax=ymaxi*1.4, xmin=xmini, xmax=xmaxi*1.1, ymin=0.95*ymini) 
ax2.tick_params(length=6, width=1, labelsize=ftl)
ax2.yaxis.get_offset_text().set_color(col)
ax2.yaxis.get_offset_text().set_fontsize(ftl)

ax2.legend(loc="upper left", fontsize=ftl)

print filepotlabs[2]+" plotted"


plt.show()

boolplot=eg.ynbox(msg="Save plots?")

saveplots(boolplot, imagefile, fig)

plt.close()

print "finished"


