import matplotlib.pyplot as plt
import numpy as np
import sys
import copy
import matplotlib
import matplotlib.ticker as ticker
import matplotlib.gridspec as gridspec
from scipy import interpolate
import tools

def ticks_format(value, index):
    """
    This function decompose value in base*10^{exp} and return a latex string.
    If 0<=value<99: return the value as it is.
    if 0.1<value<0: returns as it is rounded to the first decimal
    otherwise returns $base*10^{exp}$
    I've designed the function to be use with values for which the decomposition
    returns integers
    """
    exp = np.floor(np.log10(value))
    base = value/10**exp
    if exp == 0 or exp == 1:
        return '${0:d}$'.format(int(value))
    if exp == -1:
        return '${0:.1f}$'.format(value)
    else:
        return '${0:d}\\times10^{{{1:d}}}$'.format(int(base), int(exp))    
    
def extract_columns(data, quant=0):    
    if (quant==0 or quant==1):
        a = data[:,0]
        b = data[:,1]
        c = data[:,2]
        return a, b, c
    elif (quant==2):
        a = data[:,0]
        b = data[:,1]
        c = data[:,2]
        d = data[:,3]
        return a, b, c, d
    
def w_eq_of_state(q, q_dot, hubble=1, scale=1, camb=False):
    alpha = 10.
    V0 = 1.06e-7
    kine = 0.5*q_dot**2
    if camb:
        print len(q)
        print len(q_dot)
	print len(hubble)
	print len(scale)
        kine=kine*(hubble**2)*(scale**2)
    p = kine-V0*np.exp(-alpha*q)
    rho = kine+V0*np.exp(-alpha*q)
    w = p/rho   
    return rho, p, w
    
def z_redshift(a):
    z = (1.0/a) - 1.0
    return z

def rho_total(rho1, rho2, rho3):
    rhotot=rho1+rho2+rho3
    return rhotot

def Omega_calc(rhoi, rhotot):
    Omegai=rhoi/rhotot
    return Omegai  
    
#Base1 = './background-64-alpha9-2em4/' 
#Base2 = './background-64-alpha10-3em4/'
#Base3 = './background-64-alpha10-5em5/'
#Base4 = './background-64-alpha10-6em4/'
#Base5 = './background-64-alpha11-2em4/'
#Base6 = './background-128-alpha10-3em4/'
#Base7 = './background-128-alpha10-6em4/'
#Base8 = './background-128-alpha11-2em4/'

#Base00 = './background-64-alpha10-2em4/'
#Base01 = './background-128-alpha10-2em4/'

Base1 = './model1-camb-output/'

Base2 = './model2-camb-output/'

Base3 = './model3-camb-output/'


#simulabel1 = 'n64-alpha9-m2em4'
#simulabel2 = 'n64-alpha10-m3em4'
#simulabel3 = 'n64-alpha10-m5em5' 
#simulabel4 = 'n64-alpha10-m6em4'
#simulabel5 = 'n64-alpha11-m2em4'
#
#simulabel6 = 'n128-alpha10-m3em4'
#simulabel7 = 'n128-alpha10-m6em4'
#simulabel8 = 'n128-alpha11-m2em4'
#simulabel00 = 'n64-alpha10-m2em4'
#simulabel01 = 'n128-alpha10-m2em4'

simulabel1 = 'Camb-model1-mass'
simulabel2 = 'Camb-model2-mass'
simulabel3 = 'Camb-model3-mass'

baselist=[Base1, Base2, Base3]

simulablist = [simulabel1, simulabel2, simulabel3]

imagefile='./simulation-plots/'+simulabel1

plotwhat="Nocamb"  #choose what or not to plot

# lof = list of files
background_from = ['mass.dat']

list_of_back_files = [[base+filename for filename in background_from] for base in baselist] 

#choose data set 
data_full = [[np.loadtxt(bckfile)[:,:] for bckfile in bckfiles] for bckfiles in list_of_back_files]

#plot quantities until z=0 plotend=0 or until last value of files plotend=1
plotend=1


H0units=1.0/299792

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

G = gridspec.GridSpec(4,4)
#choose image size, dpi and background color
fig=plt.figure(1, figsize=(20,12), dpi=80,facecolor='w')
#list of default colors for plot lines
collist=['b','g','r','c','Indigo','Olive','OrangeRed','SkyBlue', 'ForestGreen', 'DarkGoldenrod', 'GoldenRod', 'Plum', 'Crimson', 'SteelBlue', 'YellowGreen']
collist2=['Indigo','OrangeRed','SkyBlue', 'ForestGreen', 'DarkGoldenrod', 'GoldenRod', 'Plum', 'Crimson', 'SteelBlue', 'YellowGreen']
lslis=['-','--',':','-.','.']
# specify plot grid axes
lslis2=lslis[::-1]

indi=0

subs = [1., 2., 4., 6., 8.]

#axes2 = fig.add_subplot(G[:2,:])


globalxmin=0.1
globalxmax=1.1



secondaxes2 = fig.add_subplot(G[1:2,:])
secondaxes3 = fig.add_subplot(G[2:3,:])
secondaxes4 = fig.add_subplot(G[3:4,:])
indi=0
#for a, m_bar_nu in zip(a_s, m_bar_nu_s):

secondaxes2.semilogx(data_full[0][0][:,0], data_full[0][0][:,1], lslis[indi], label=r'$mod1-m_{\nu}$', color=collist2[indi], lw=3)
secondaxes3.semilogx(data_full[1][0][:,0], data_full[1][0][:,1], lslis[indi], label=r'$mod2-m_{\nu}$', color=collist2[indi+1], lw=3)
secondaxes4.semilogx(data_full[2][0][:,0], data_full[2][0][:,1], lslis[indi], label=r'$mod3-m_{\nu}$', color=collist2[indi+2], lw=3)

    
xmini,xmaxi,ymini,ymaxi = secondaxes2.axis('tight')
xmini,xmaxi,ymini3,ymaxi3 = secondaxes3.axis('tight')
xmini,xmaxi,ymini4,ymaxi4 = secondaxes4.axis('tight')
secondaxes2.axis(ymax=ymaxi*1.1, xmin=1, xmax=10, ymin=ymini)
secondaxes3.axis(ymax=ymaxi3*1.1, xmin=1, xmax=10, ymin=ymini3)
secondaxes4.axis(ymax=ymaxi4*1.1, xmin=1, xmax=10, ymin=ymini4)

secondaxes2.legend(loc='best',markerscale=1.0,prop={'size':15},numpoints=3,handlelength=3)
secondaxes3.legend(loc='best',markerscale=1.0,prop={'size':15},numpoints=3,handlelength=3)
secondaxes4.legend(loc='best',markerscale=1.0,prop={'size':15},numpoints=3,handlelength=3)


secondaxes2.grid(True,which="major",ls=":")
secondaxes2.tick_params(length=6, width=1)
secondaxes2.set_ylabel(r"$\bar m_{\nu} $",size='x-large')
secondaxes3.set_ylabel(r"$\bar m_{\nu} $",size='x-large')
secondaxes4.set_ylabel(r"$\bar m_{\nu} $",size='x-large')
secondaxes2.xaxis.set_label_position('bottom')
secondaxes4.set_xlabel(r"$1+z$",size='x-large')


print "m_nu plotted"



fig.savefig(imagefile,dpi=200)

plt.show()


###--possibly useful code--#

#axes1.text(0.02,0.1,'z=0',verticalalignment='top',horizontalalignment='left',transform=axes1.transAxes,fontsize=14,style='italic') 
#axes1.xaxis.set_minor_locator(ticker.LogLocator(subs=subs))
#axes1.xaxis.set_major_formatter(ticker.NullFormatter())
#axes1.xaxis.set_minor_formatter(ticker.FuncFormatter(ticks_format))
#axes1.xaxis.set_minor_formatter(ticker.LogFormatter(base=10.0, labelOnlyBase=False))
#minorLocator   = ticker.LogLocator(0.1,[0.01,0.02])
#axes1.xaxis.set_minor_locator(minorLocator)
#axes1.set_xticks(np.linspace(0.5,1.0,5))
#axes1.set_xticklabels(('0.1','0.2','0.4','0.5','0.6','0.7','0.8''0.9'))

###quantity=0 #0=general, 1=quintessence, 2=neutrinos
###delta_a = 0.01
###a_init = 0.02
###a_fin = 1.0
###a_values = np.arange(int(a_init*100)+1, int(a_fin*100), int(delta_a*100)) # or +1
###list_zerofill = [str(av).zfill(3) for av in a_values ]
###list_snaps = [aval.rstrip('0') for aval in list_zerofill]


