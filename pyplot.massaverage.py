import matplotlib.pyplot as plt
import numpy as np
import itertools
import sys
import copy
import matplotlib
import matplotlib.ticker as ticker
import matplotlib.gridspec as gridspec
import easygui as eg
from scipy import interpolate
import tools
import mycosmotools
from simusBaseDirs import *
from plotStyles import *
import glob

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

G = gridspec.GridSpec(4,4)
#choose image size, dpi and background color
fig=plt.figure(1, figsize=(20,12), dpi=80,facecolor='w')

#globalxmin=0.1
#globalxmax=1.1

Base = '../backreaction/data-tables-for-plotting/'

shortlabel = ['mod1','mod2']
simulab = [simulabel1, simulabel2]
simulab = ['model M1', 'model M2' ]

model = ['model1','model2']
quantities = ['mnu']
#sources = ['CAMB','gnq']
#soulabels = ['nuCAMB','gnqsim']
sources = ['gnq']
formattxt = '.txt'

qlabels = [r"$m_\nu$"]

ml = len(model)
ql = len(quantities)


dataqu = np.empty([ml,ql], dtype=object)
filequ = np.empty([ml,ql], dtype=object)

#filequ = [['1','b'],['a','c']]

for si,s in enumerate(sources):
    for mi,m in enumerate(model):
        for qi, q in enumerate(quantities):
            filequ[mi][qi] = (glob.glob(Base+s+'-'+m+'*'+'-'+q+formattxt))[0]
            dataqu[mi,qi] = np.loadtxt(filequ[mi][qi])[:,:]



imagefile='../paper-simulation-plots/'+'Background-'+'model-'.join(model)+'-'+'-And-'.join(sources)+'_'+'-and-'.join(quantities)+'-v1'


print "printing image file: "+imagefile

#globalxmin=0.1
#globalxmax=1.1

ftl=35 #fontsizeleg
ftt = 40

axes0 = fig.add_subplot(G[:,:])
lwid=4
rmsx1 = 0.8
rmsx2 = 1.0
mnuRMSm1 = 0.120274
mnuRMSm2 = 0.164403
mnurmsmods = [mnuRMSm1,mnuRMSm2]

rmslabs = ["M1, "+r"$ \langle m_{\nu} \rangle(a_l) =0.120$", "M2, "+r"$\langle m_{\nu} \rangle(a_l) = 0.164$"]

plot_pot_lines=[]

colsimp=['peru','navy']
customdashes = [20,10]

for i in range(ml):
    lini=lslis[i]
    indi = 0
    for j in range(ql): 
        col=colstand2[i] 
        linp1, = axes0.plot(dataqu[i,j][:,0], dataqu[i,j][:,1], lini, label=simulab[i], color=col, lw=lwid)
        axes0.plot((rmsx1,rmsx2),(mnurmsmods[i], mnurmsmods[i]), '-', dashes=customdashes, label=rmslabs[i], color=colsimp[i], lw=5)
        indi = indi + 1 
      
        plot_pot_lines.append([linp1])


axes0.legend(loc='upper left', ncol=1, prop={'size':ftl}, handlelength=3.5) 

xmini,xmaxi,ymini,ymaxi = axes0.axis('tight')
print xmini, xmaxi
axes0.axis(ymax=0.6, xmin=0.6, xmax=1.02, ymin=0.) 


axes0.grid(True,which="major",ls=":")
axes0.tick_params(length=6, width=1, labelsize=ftl, labelright='off', labelleft='on')
axes0.set_ylabel(r"neutrino mass  $\bar{m}_{\nu}$",fontsize=ftt)
axes0.set_xlabel(r"scale factor $a$",fontsize=ftt)
axes0.xaxis.set_label_position('bottom')
axes0.tick_params(axis='x', pad=10)

#axes0.annotate(simulab, xy=(0.5, 0.96), xycoords='axes fraction', ha="center", va="center", size=25, bbox=bbox_props)

##axes0.set_title("Model "+simulablist[0])

print "-plotted-".join(simulab)


plt.show()

boolplot=eg.ynbox(msg="Save plots?")

saveplots(boolplot,imagefile,fig)

plt.close()

print "finished"
