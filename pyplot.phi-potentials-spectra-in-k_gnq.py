import matplotlib.pyplot as plt
import numpy as np
import itertools
import sys
import copy
import matplotlib
import matplotlib.ticker as ticker
import matplotlib.gridspec as gridspec
from scipy import interpolate
import tools
import mycosmotools
from simusBaseDirs import *
import easygui as eg
from plotStyles import *


shortlabel1 = 'mod1'
shortlabel2 = 'mod2'
shortlabel3 = 'mod3'


baselist=[Base1, Base2, Base3]

simulabel2="M2, "+r"$ \langle m_{\nu} \rangle (a_e) =0.07$"
simulabel3="M5, "+r"$ \langle m_{\nu} \rangle (a_e)  =0.40 $"
simulablist = [simulabel1, simulabel2, simulabel3]
simulablistfn = ['M2', 'M5']
shortlablist = [shortlabel1, shortlabel2, shortlabel3]

#choose here using indices, which simulations to plot together or separately
pt=[1,2]

baselist=[v for indi,v in enumerate(baselist) if indi in pt]

simulablist=[v for indi,v in enumerate(simulablist) if indi in pt]
shortlablist=[v for indi,v in enumerate(shortlablist) if indi in pt]




a_times = np.array(range(4000,10500,500))
avail_times_full=[str(ii).zfill(6) for ii in a_times]
scalefacts= a_times/10000.0
scalefactstr=['a='+str(aa) for aa in scalefacts]
scalefactdict = dict(zip(avail_times_full,scalefacts))


times_to_plot=[0,5]

avail_times_chosen=[tt for indi,tt in enumerate(avail_times_full) if indi in times_to_plot]

# lof = list of files
potential_tot_files=['potspec_'+at+'_phi.dat' for at in avail_times_chosen]
potential_nu_files=['potspec_'+at+'_phi_nu.dat' for at in avail_times_chosen]
potential_m_files=['potspec_'+at+'_phi_m.dat' for at in avail_times_chosen]

potentials_files=[potential_tot_files, potential_nu_files, potential_m_files]
potlabs=[r'$\Phi_t$',r'$\Phi_{\nu}$',r'$\Phi_m$']
#plot which potentials?
pwp=[0,1]

filepotlabs=['Phit','Phinu','Phim'] 
filepotlabs=[v for indi,v in enumerate(filepotlabs) if indi in pwp]


imagefile='../paper-simulation-plots/'+'Phi-GravPotSpec-'+'-and-'.join(simulablistfn)+'-at-'+avail_times_chosen[-1]+'-'+"_".join(filepotlabs)+'-new'



list_of_files = [[[base+filename for filename in pot] for pot in potentials_files] for base in baselist] 

#choose data set 
data_full = [[[np.loadtxt(filet)[:,:] for filet in filespm] for filespm in filemod] for filemod in list_of_files]        


H0units=1.0/299792


G = gridspec.GridSpec(4,4)

plt.rc('text', usetex=True)
plt.rc('font', family='serif')



#choose image size, dpi and background color

fig=plt.figure(1, figsize=(20,12), dpi=80,facecolor='w')

indi=0

subs = [1., 2., 4., 6., 8.]

#axes2 = fig.add_subplot(G[:2,:])
lwid=4

globalxmin=0.1
globalxmax=1.1

ftl=30 #fontsizeleg
ftt=40

secondaxes0 = fig.add_subplot(G[0:2,:])
secondaxes1 = fig.add_subplot(G[2:4,:], sharey=secondaxes0)
indi=0
plot_pot_lines=[]
for asctime, plot1snap, plot2snap in zip(avail_times_chosen, data_full[0][pwp[0]], data_full[0][pwp[1]]):
    col=colstand2[1]
    lini=lslis[indi]
    linp1, = secondaxes0.loglog(plot1snap[:,0], plot1snap[:,1], lini, label=potlabs[pwp[indi]], color=col, lw=lwid) 
    col=colstand2[0]
    linp2, = secondaxes0.loglog(plot2snap[:,0], plot2snap[:,1], lini, color=col, lw=lwid) 
    if (indi==0):
        plot_pot_lines.append([linp1,linp2])
    indi = indi+1

phiArtist1 = plt.Line2D((0,1),(0,0), color='dimgray', linestyle=lslis[0], lw=lwid)
phiArtist2 = plt.Line2D((0,1),(0,0), color='dimgray', linestyle=lslis[1], lw=lwid)

legend_pot11=secondaxes0.legend([phiArtist1, phiArtist2], [scalefactdict[ii] for ii in avail_times_chosen], loc='upper left', ncol=len(avail_times_chosen),prop={'size':ftl}, handlelength=3, 
borderaxespad=0.0) 
legg=secondaxes0.legend(plot_pot_lines[0], [potlabs[pwp[0]], potlabs[pwp[1]]], loc='best',markerscale=1.0,prop={'size':ftl},numpoints=3,handlelength=3.5)
secondaxes0.add_artist(legend_pot11)


for l in legg.legendHandles:            
    l.set_linewidth(10)

xmini,xmaxi,ymini,ymaxi = secondaxes0.axis('tight')
print xmini, xmaxi
secondaxes0.axis(ymax=1.1*10.0**-4, xmin=0.02, xmax=1.0, ymin=0.9*10.0**-9) 
secondaxes0.grid(True,which="major",ls=":")
secondaxes0.tick_params(length=10, width=1.5, labelsize=ftl, labelright='on', labelleft='off', which='major')
secondaxes0.tick_params(length=4, width=1,  which='minor')
secondaxes0.get_xaxis().set_tick_params(which='both', direction='inout')
secondaxes0.get_xaxis().set_tick_params(which='minor', length=7)
secondaxes0.xaxis.tick_top()

secondaxes0.annotate(simulablist[0], xy=(0.15, 0.5), xycoords='axes fraction', ha="center", va="center", size=30, bbox=bbox_props)

#secondaxes0.set_xlabel(r"$k \;[\textrm{h/Mpc}]$",fontsize=ftt)
#secondaxes0.xaxis.set_label_position('top')

print simulablist[0]+" plotted"

indi=0
plot_pot_lines=[]
for asctime, plot1snap, plot2snap in zip(avail_times_chosen, data_full[1][pwp[0]], data_full[1][pwp[1]]):
    col=colstand2[1]
    lini=lslis[indi]
    linp1, = secondaxes1.loglog(plot1snap[:,0], plot1snap[:,1], lini, label=potlabs[pwp[indi]], color=col, lw=lwid) 
    col=colstand2[0]
    linp2, = secondaxes1.loglog(plot2snap[:,0], plot2snap[:,1], lini, color=col, lw=lwid) 
    if (indi==0):
        plot_pot_lines.append([linp1,linp2])
    indi = indi + 1


legend_pot22=secondaxes1.legend([phiArtist1, phiArtist2], [scalefactdict[ii] for ii in avail_times_chosen], loc='lower left', ncol=len(avail_times_chosen),prop={'size':ftl}, handlelength=3, borderaxespad=0.) 
legg=secondaxes1.legend(plot_pot_lines[0], [potlabs[pwp[0]], potlabs[pwp[1]]], loc='best',markerscale=1.0,prop={'size':ftl},numpoints=3,handlelength=3.5)
secondaxes1.add_artist(legend_pot22)

for l in legg.legendHandles:            
    l.set_linewidth(10)

xmini,xmaxi,ymini,ymaxi = secondaxes1.axis('tight')
secondaxes1.axis(ymax=1.1*10.0**-4, xmin=0.02, xmax=1.0, ymin=0.9*10.0**-9) 
secondaxes1.grid(True,which="major",ls=":")
secondaxes1.tick_params(length=10, width=1.5, labelsize=ftl, labelright='off', labelleft='on', which='major')
secondaxes1.tick_params(length=4, width=1,  which='minor')
secondaxes1.get_xaxis().set_tick_params(which='both', direction='inout')
secondaxes1.get_xaxis().set_tick_params(which='both', direction='inout')
secondaxes1.get_xaxis().set_tick_params(which='minor', length=7)
secondaxes1.set_ylabel(r"Gravitational Potential $\bar \Phi(k)$",fontsize=ftl)
secondaxes1.set_xlabel(r"$k \;[\textrm{h/Mpc}]$",fontsize=ftt)
secondaxes1.xaxis.set_label_position('bottom')

secondaxes1.annotate(simulablist[1], xy=(0.15, 0.5), xycoords='axes fraction', ha="center", va="center", size=30, bbox=bbox_props)



yy=plt.ylabel(r"Gravitational Potential $\bar \Phi(k)$",fontsize=ftt)
yy.set_position((yy.get_position()[0],1))

plt.subplots_adjust(hspace=0.09)

print simulablist[1]+" plotted"
print "phis plotted"


plt.show()

boolplot=eg.ynbox(msg="Save plots?")

if (boolplot==True):
    fig.savefig(imagefile+".png",dpi=400, bbox_inches='tight')
    print "saving png"
    fig.savefig(imagefile+".pdf",dpi=400, bbox_inches='tight')
    print "saving pdf"
    fig.savefig(imagefile+".eps",format="eps", dpi=1000, bbox_inches='tight')
    print "saving ps"
else:
    print "no plots saved"


plt.close()

print "finished"


