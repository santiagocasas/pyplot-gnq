Base1 = '../GNQ-VaryingBeta-Master-CorrectFactor-model1-laterstart-a1p0/'
Base2 = '../GNQ-VaryingBeta-Master-CorrectFactor-model2-laterstart-a1p02/'
Base3 = '../GNQ-VaryingBetaMaster-CorrectFactor-model3/'
#Base4 = '../GNQ-Mc-vb-64-AllFields-model4-alfa10-a0p94/'
#Base5 = '../GNQ-Mc-vb-64-AllFields-model5-smallerstep/'
#Base6 = '../GNQ-Mc-vb-128-model2-all-a0p99/'

#Base7 = '../GNQ-Mc-vb-model2-laterstart-a1p01/'
#Base8 = '../GNQ-VaryingBeta-Master-CorrectFactor-model2-laterstart-a0p85/'


simulabel1 = 'gnq-M0.06'
simulabel2 = 'gnq-M0.4'
simulabel3 = 'gnq-M1.0'
simulabel4 = 'GNQ-64-mod4'
simulabel5 = 'GNQ-64-mod5'
simulabel6 = 'GNQ-128-mod2'
simulabel7 = 'GNQ-64-mod2-ls'
simulabel8 = 'GNQ-64-mod2-cfls'

