#!/bin/bash

dir=$2

runtype=$1

if [ $1 == "-help" ]; then
	echo "Usage: digit-renamer.sh [-help -v -n] directory"
	echo "-help: Display this help."
	echo "-n: dry test run, do not rename, just test"
	echo "-v: real run, with verbose output"
	echo "If no argument is given, default: -n"
	exit 1
fi

if [[ -d $1 ]]; then
	echo " Running in test mode"
	runtype="-n"
	dir=$1
fi
if [ $# == 2 ]
then
      if [[ "$1" != "-help" && "$1" != "-n" && "$1" != "-v" ]]
      then
	echo "Invalid arguments passed, use -help for usage message."
	exit 1
      fi
fi


if [[ -n $2 && ! -d $2 ]]; then
	echo "Directory non-existing"
	exit 1
fi


echo "Moving to Directory $dir"

cd $dir

#interesting rename mathcing:  rename -v 's/-a0p(\d{2})(r?)\.png/-a0p${1}0${2}\.png/' model1-*  capture second group if there is an optional "r" after 2 digits.

echo " Operation: Add one zero to two-digit numbers"
rename $runtype 's/_(\d{2})_/_${1}0_/' *

echo " Operation: Add two zeros to one-digit numbers"

rename $runtype 's/_(\d{1})_/_${1}00_/' *
